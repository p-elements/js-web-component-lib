var FILE_ENCODING = 'utf-8';
var EOL = '\n';

var fs = require('fs');

function deleteFolderRecursive(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
};

function concat(opts, callback) {

    var fileList = opts.src;
    var distPath = opts.dest;
    var out = fileList.map(function(filePath) {
        return fs.readFileSync(filePath, FILE_ENCODING);
    });

    fs.writeFileSync(distPath, out.join(EOL), FILE_ENCODING);

    if (callback) {
        callback();
    }
}

function uglifyAndPrepend(srcPath, distPath, prependFile) {
    var prepend = '';
    if(prependFile){
     prepend = fs.readFileSync(prependFile, FILE_ENCODING);
    }
    var uglyfyJS = require('uglify-js');
    var result = uglyfyJS.minify(srcPath, {
        mangle: true,
        compress: {
            sequences: true,
            dead_code: true,
            conditionals: true,
            booleans: true,
            unused: true,
            if_return: true,
            join_vars: true,
            drop_console: true
        }
    });
    fs.writeFileSync(distPath, prepend + result.code, FILE_ENCODING);

}

function copyFileSync (srcFile, destFile) {
  var BUF_LENGTH, buff, bytesRead, fdr, fdw, pos;
  BUF_LENGTH = 64 * 1024;
  buff = new Buffer(BUF_LENGTH);
  fdr = fs.openSync(srcFile, 'r');
  fdw = fs.openSync(destFile, 'w');
  bytesRead = 1;
  pos = 0;
  while (bytesRead > 0) {
    bytesRead = fs.readSync(fdr, buff, 0, BUF_LENGTH, pos);
    fs.writeSync(fdw, buff, 0, bytesRead);
    pos += bytesRead;
  }
  fs.closeSync(fdr);
  return fs.closeSync(fdw);
};


deleteFolderRecursive('./dist');

fs.mkdirSync('./dist');

concat({
        src: [
            'js/ie-lte-9.js',
            './js/dom-class.js',
            './js/dom4.js',
            './js/document-register-element.js',
            './js/restyle.js',
            './js/es-class.js',
            './js/bindings.js',
            './js/core-promise.js'
        ],
        dest: './dist/js-web-component-lib.js'
    },
    function() {
        uglifyAndPrepend('./dist/js-web-component-lib.js', './dist/js-web-component-lib.min.js', './js/ie-lte-9.js');
    }
);
