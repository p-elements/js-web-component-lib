/*! IE LTE 9 ONLY *//*@cc_on (function(f){window.setTimeout=f(window.setTimeout);window.setInterval=f(window.setInterval)})(function(f){return function(c,t){var a=[].slice.call(arguments,2);return f(function(){c.apply(this,a)},t)}}); @*/

var DOMClass = (function (g, A, O) {'use strict';
  var
    ATTACHED = 'onAttached',
    ATTACHED_CALLBACK = 'attachedCallback',
    CHANGED = 'onChanged',
    CHANGED_CALLBACK = 'attributeChangedCallback',
    CONSTRUCTOR = 'constructor',
    CONSTRUCTOR_CALLBACK = 'createdCallback',
    CSS = 'css',
    STYLE = '<style>',
    STYLESHEET = 'stylesheet',
    DETACHED = 'onDetached',
    DETACHED_CALLBAK = 'detachedCallback',
    EXTENDS = 'extends',
    NAME = 'name',
    document = g.document,
    html = document.documentElement,
    hOP = O.hasOwnProperty,
    empty = A.prototype,
    copyOwn = function (source, target) {
      for (var k, p = gOK(source), i = p.length; i--;) {
        k = p[i];
        if (ignore.indexOf(k) < 0 && !hOP.call(target, k)) {
          dP(target, k, gOPD(source, k));
        }
      }
    },
    dP = O.defineProperty,
    gOPD = O.getOwnPropertyDescriptor,
    gOPN = O.getOwnPropertyNames || O.keys || function (o) {
      var a = [], k;
      for (k in o) if (hOP.call(o, k)) a.push(k);
      return a;
    },
    gOPS = O.getOwnPropertySymbols || function () {
      return empty;
    },
    getHTMLConstructor = function (name) {
      return g['HTML' + name + 'Element'];
    },
    gOK = function (obj) {
      return gOPS(obj).concat(gOPN(obj));
    },
    grantArguments = function (el, args) {
      if (!args.length) {
        var attr = el.getAttribute('data-arguments');
        if (attr) {
          args = attr.charAt(0) === '[' ?
            JSON.parse(attr) :
            attr.split(/\s*,\s*/);
        }
      }
      return args;
    },
    ignore = gOK(function () {}),
    setIfThere = function  (where, what, target, alias) {
      if (hOP.call(where, what)) {
        target[alias] = where[what];
      }
    },
    // WUT? https://gist.github.com/WebReflection/4327762cb87a8c634a29
    slice = function slice() {
      for (var
        o = +this,
        i = o,
        l = arguments.length,
        n = l - o,
        a = new A(n < 0 ? 0 : n);
        i < l; i++
      ) a[i - o] = arguments[i];
      return a;
    },
    uid = function (name) {
      return name + '-i-' + (
        hOP.call(uids, name) ? ++uids[name] : (uids[name] = 0)
      );
    },
    lazyStyle = function (el, key, uniqueClassId) {
      var style;
      el.setAttribute('dom-class-uid', uniqueClassId);
      dP(el, CSS, {
        configurable: true,
        get: function () {
          return style || (style = restyle(
            key + '[dom-class-uid="' + uniqueClassId + '"]', {}
          ));
        },
        set: function (info) {
          el[CSS].replace(info);
        }
      });
    },
    loadStyle = function (prefix, source) {
      var
        xhr = new XMLHttpRequest(),
        where = document.body ||
                document.head ||
                html,
        css,
        style
      ;
      try {
        xhr.open('GET', source, false);
        xhr.send(null);
        css = xhr.responseText;
        style = where.insertBefore(
          document.createElement('style'),
          where.lastChild
        );
        style.type = 'text/css';
        if ('styleSheet' in style) {
          style.styleSheet.cssText = css;
        } else {
          style.appendChild(document.createTextNode(css));
        }
      } catch(deprecated) {
        style = document.createElement('link');
        style.href  = source;
        style.rel   = 'stylesheet';
        style.type  = 'text/css';
        where.insertBefore(style, where.lastChild);
      }
      return html.offsetWidth;
    },
    // preFix = function (prefix, css) {
    //   var
    //     _ = function (selectors) {
    //       var s = selectors.split(/\s*,\s*/), i = s.length;
    //       while (i--) s[i] = prefix + ' ' + trim.call(s[i]);
    //       return s.join(',');
    //     },
    //     i = 0,
    //     length = css.length,
    //     out = [],
    //     open, close
    //   ;
    //   while (i < length) {
    //     open = css.indexOf('{', i);
    //     if (open < 0) break;
    //     close = css.indexOf('}', open) + 1;
    //     out.push(_(css.slice(i, open)), css.slice(open, close));
    //     i = close;
    //   }
    //   return out.join('');
    // },
    // trim = ''.trim || function () {
    //   return this.replace(/^\s+|\s+/g, '');
    // },
    uids = {},
    i = 0
  ;
  return function DOMClass(description) {
    var
      CustomElement = function CustomElement() {
        if (hasStyleSheet) {
          hasStyleSheet = false;
          loadStyle(key, styleSheet);
        }
        args = slice.apply(0, arguments);
        return new Element();
      },
      args = empty,
      el = {},
      css = hOP.call(description, CSS),
      init = hOP.call(description, CONSTRUCTOR),
      hasStyleSheet = hOP.call(description, STYLESHEET),
      styleSheet = hasStyleSheet && description[STYLESHEET],
      createdCallback = init && description[CONSTRUCTOR],
      Element,
      constructor,
      key, proto, nodeName
    ;
    setIfThere(description, ATTACHED, el, ATTACHED_CALLBACK);
    setIfThere(description, CHANGED, el, CHANGED_CALLBACK);
    setIfThere(description, DETACHED, el, DETACHED_CALLBAK);
    for (key in description) {
      if (hOP.call(description, key)) {
        switch (key) {
          case ATTACHED:
          case CHANGED:
          case CONSTRUCTOR:
          case DETACHED:
          case EXTENDS:
          case NAME:
          case CSS:
          case STYLESHEET:
            break;
          default:
            el[key] = description[key];
            break;
        }
      }
    }
    el[EXTENDS] = hOP.call(description, EXTENDS) ?
      description[EXTENDS].prototype :
      HTMLElement.prototype
    ;
    if (el[EXTENDS] instanceof HTMLElement) {
      // dumbest thing I've possibly ever written, right here!
      //  Object.getOwnPropertyNames(this).filter((k) => {return k.slice(0, 4)==='HTML'}).map((k)=>{return k.slice(4, -7)}).sort();
      //  ["", "AllCol", "Anchor", "Applet", "Area", "Audio", "BR", "Base", "Body", "Button", "Canvas", "Col", "Content",
      //  "D", "DList", "DataList", "Details", "Dialog", "Directory", "Div", "Embed", "FieldSet", "Font", "Form", "FormControlsCol",
      //  "Frame", "FrameSet", "HR", "Head", "Heading", "Html", "IFrame", "Image", "Input", "Keygen", "LI", "Label", "Legend",
      //  "Link", "Map", "Marquee", "Media", "Menu", "Meta", "Meter", "Mod", "OList", "Object", "OptGroup", "Option", "OptionsCol",
      //  "Output", "Paragraph", "Param", "Picture", "Pre", "Progress", "Quote", "Script", "Select", "Shadow", "Source", "Span",
      //  "Style", "Table", "TableCaption", "TableCell", "TableCol", "TableRow", "TableSection", "Template", "TextArea", "Title",
      //  "Track", "UList", "Unknown", "Video"]
      // but I'll enable only most common one ... please file a bug if you need others
      // also, if you know a way to retrieve a nodeName via its constructor please shout it to me!
      switch (description[EXTENDS]) {
        case getHTMLConstructor('Anchor'): nodeName = 'a'; break;
        case getHTMLConstructor('Audio'): nodeName = 'audio'; break;
        case getHTMLConstructor('BR'): nodeName = 'br'; break;
        case getHTMLConstructor('Body'): nodeName = 'body'; break;
        case getHTMLConstructor('Button'): nodeName = 'button'; break;
        case getHTMLConstructor('Canvas'): nodeName = 'canvas'; break;
        case getHTMLConstructor('Col'): nodeName = 'col'; break;
        case getHTMLConstructor('DataList'): nodeName = 'dl'; break;
        case getHTMLConstructor('Div'): nodeName = 'div'; break;
        case getHTMLConstructor('Form'): nodeName = 'form'; break;
        case getHTMLConstructor('HR'): nodeName = 'hr'; break;
        case getHTMLConstructor('Head'): nodeName = 'head'; break;
        case getHTMLConstructor('IFrame'): nodeName = 'iframe'; break;
        case getHTMLConstructor('Image'): nodeName = 'img'; break;
        case getHTMLConstructor('Input'): nodeName = 'input'; break;
        case getHTMLConstructor('LI'): nodeName = 'li'; break;
        case getHTMLConstructor('Label'): nodeName = 'label'; break;
        case getHTMLConstructor('Legend'): nodeName = 'legend'; break;
        case getHTMLConstructor('Link'): nodeName = 'link'; break;
        case getHTMLConstructor('Menu'): nodeName = 'menu'; break;
        case getHTMLConstructor('OList'): nodeName = 'ol'; break;
        case getHTMLConstructor('Option'): nodeName = 'option'; break;
        case getHTMLConstructor('Paragraph'): nodeName = 'p'; break;
        case getHTMLConstructor('Progress'): nodeName = 'progress'; break;
        case getHTMLConstructor('Quote'): nodeName = 'quote'; break;
        case getHTMLConstructor('Select'): nodeName = 'select'; break;
        case getHTMLConstructor('Span'): nodeName = 'span'; break;
        case getHTMLConstructor('Style'): nodeName = 'style'; break;
        case getHTMLConstructor('Table'): nodeName = 'table'; break;
        case getHTMLConstructor('TableCaption'): nodeName = 'caption'; break;
        case getHTMLConstructor('TableCell'): nodeName = 'td'; break;
        case getHTMLConstructor('TableCol'): nodeName = 'colgroup'; break;
        case getHTMLConstructor('TableRow'): nodeName = 'tr'; break;
        case getHTMLConstructor('TableSection'): nodeName = 'tbody'; break;
        case getHTMLConstructor('Table'): nodeName = 'table'; break;
        case getHTMLConstructor('Table'): nodeName = 'table'; break;
        case getHTMLConstructor('TextArea'): nodeName = 'textarea'; break;
        case getHTMLConstructor('Track'): nodeName = 'track'; break;
        case getHTMLConstructor('UList'): nodeName = 'ul'; break;
        case getHTMLConstructor('Video'): nodeName = 'video'; break;
        // GZIP ALL THE THINGS!
      }
    }
    key = hOP.call(description, NAME) ? description[NAME] : ('x-dom-class-' + i++);
    if (css) el[STYLE] = restyle(key, description[CSS]);
    el[CONSTRUCTOR_CALLBACK] = function () {
      var a = grantArguments(this, args);
      args = empty;
      constructor.apply(this, a);
      if (css) lazyStyle(this, key, uid(key));
      if (init) createdCallback.apply(this, a);
    };
    constructor = new Class(el);
    copyOwn(constructor, CustomElement);
    proto = {prototype: constructor.prototype};
    // if we are extending an HTML element
    // we should retrieve a name and an `is` property
    if (nodeName) proto[EXTENDS] = nodeName;
    Element = document.registerElement(key, proto);
    CustomElement.prototype = Element.prototype;
    dP(Element.prototype, CONSTRUCTOR, {value: CustomElement});
    return CustomElement;
  };
}((this && this.window) || global, Array, Object));
/*!
Copyright (C) 2013-2015 by Andrea Giammarchi - @WebReflection

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/
(function(window){'use strict';
  /* jshint loopfunc: true, noempty: false*/
  // http://www.w3.org/TR/dom/#element

  function createDocumentFragment() {
    return document.createDocumentFragment();
  }

  function createElement(nodeName) {
    return document.createElement(nodeName);
  }

  function enoughArguments(length, name) {
    if (!length) throw new Error(
      'Failed to construct ' +
        name +
      ': 1 argument required, but only 0 present.'
    );
  }

  function mutationMacro(nodes) {
    if (nodes.length === 1) {
      return textNodeIfString(nodes[0]);
    }
    for (var
      fragment = createDocumentFragment(),
      list = slice.call(nodes),
      i = 0; i < nodes.length; i++
    ) {
      fragment.appendChild(textNodeIfString(list[i]));
    }
    return fragment;
  }

  function textNodeIfString(node) {
    return typeof node === 'string' ? document.createTextNode(node) : node;
  }

  for(var
    head,
    property,
    TemporaryPrototype,
    TemporaryTokenList,
    wrapVerifyToken,
    document = window.document,
    hOP = Object.prototype.hasOwnProperty,
    defineProperty = Object.defineProperty || function (object, property, descriptor) {
      if (hOP.call(descriptor, 'value')) {
        object[property] = descriptor.value;
      } else {
        if (hOP.call(descriptor, 'get'))
          object.__defineGetter__(property, descriptor.get);
        if (hOP.call(descriptor, 'set'))
          object.__defineSetter__(property, descriptor.set);
      }
      return object;
    },
    indexOf = [].indexOf || function indexOf(value){
      var length = this.length;
      while(length--) {
        if (this[length] === value) {
          break;
        }
      }
      return length;
    },
    // http://www.w3.org/TR/domcore/#domtokenlist
    verifyToken = function (token) {
      if (!token) {
        throw 'SyntaxError';
      } else if (spaces.test(token)) {
        throw 'InvalidCharacterError';
      }
      return token;
    },
    DOMTokenList = function (node) {
      var
        noClassName = typeof node.className === 'undefined',
        className = noClassName ?
          (node.getAttribute('class') || '') : node.className,
        isSVG = noClassName || typeof className === 'object',
        value = (isSVG ?
          (noClassName ? className : className.baseVal) :
          className
        ).replace(trim, '')
      ;
      if (value.length) {
        properties.push.apply(
          this,
          value.split(spaces)
        );
      }
      this._isSVG = isSVG;
      this._ = node;
    },
    classListDescriptor = {
      get: function get() {
        return new DOMTokenList(this);
      },
      set: function(){}
    },
    uid = 'dom4-tmp-'.concat(Math.random() * +new Date()).replace('.','-'),
    trim = /^\s+|\s+$/g,
    spaces = /\s+/,
    SPACE = '\x20',
    CLASS_LIST = 'classList',
    toggle = function toggle(token, force) {
      if (this.contains(token)) {
        if (!force) {
          // force is not true (either false or omitted)
          this.remove(token);
        }
      } else if(force === undefined || force) {
        force = true;
        this.add(token);
      }
      return !!force;
    },
    DocumentFragmentPrototype = window.DocumentFragment && DocumentFragment.prototype,
    Node = window.Node,
    NodePrototype = (Node || Element).prototype,
    CharacterData = window.CharacterData || Node,
    CharacterDataPrototype = CharacterData && CharacterData.prototype,
    DocumentType = window.DocumentType,
    DocumentTypePrototype = DocumentType && DocumentType.prototype,
    ElementPrototype = (window.Element || Node || window.HTMLElement).prototype,
    HTMLSelectElement = window.HTMLSelectElement || createElement('select').constructor,
    selectRemove = HTMLSelectElement.prototype.remove,
    ShadowRoot = window.ShadowRoot,
    SVGElement = window.SVGElement,
    // normalizes multiple ids as CSS query
    idSpaceFinder = / /g,
    idSpaceReplacer = '\\ ',
    createQueryMethod = function (methodName) {
      var createArray = methodName === 'querySelectorAll';
      return function (css) {
        var a, i, id, query, nl, selectors, node = this.parentNode;
        if (node) {
          for (
            id = this.getAttribute('id') || uid,
            query = id === uid ? id : id.replace(idSpaceFinder, idSpaceReplacer),
            selectors = css.split(','),
            i = 0; i < selectors.length; i++
          ) {
            selectors[i] = '#' + query + ' ' + selectors[i];
          }
          css = selectors.join(',');
        }
        if (id === uid) this.setAttribute('id', id);
        nl = (node || this)[methodName](css);
        if (id === uid) this.removeAttribute('id');
        // return a list
        if (createArray) {
          i = nl.length;
          a = new Array(i);
          while (i--) a[i] = nl[i];
        }
        // return node or null
        else {
          a = nl;
        }
        return a;
      };
    },
    addQueryAndAll = function (where) {
      if (!('query' in where)) {
        where.query = ElementPrototype.query;
      }
      if (!('queryAll' in where)) {
        where.queryAll = ElementPrototype.queryAll;
      }
    },
    properties = [
      'matches', (
        ElementPrototype.matchesSelector ||
        ElementPrototype.webkitMatchesSelector ||
        ElementPrototype.khtmlMatchesSelector ||
        ElementPrototype.mozMatchesSelector ||
        ElementPrototype.msMatchesSelector ||
        ElementPrototype.oMatchesSelector ||
        function matches(selector) {
          var parentNode = this.parentNode;
          return !!parentNode && -1 < indexOf.call(
            parentNode.querySelectorAll(selector),
            this
          );
        }
      ),
      'closest', function closest(selector) {
        var parentNode = this, matches;
        while (
          // document has no .matches
          (matches = parentNode && parentNode.matches) &&
          !parentNode.matches(selector)
        ) {
          parentNode = parentNode.parentNode;
        }
        return matches ? parentNode : null;
      },
      'prepend', function prepend() {
        var firstChild = this.firstChild,
            node = mutationMacro(arguments);
        if (firstChild) {
          this.insertBefore(node, firstChild);
        } else {
          this.appendChild(node);
        }
      },
      'append', function append() {
        this.appendChild(mutationMacro(arguments));
      },
      'before', function before() {
        var parentNode = this.parentNode;
        if (parentNode) {
          parentNode.insertBefore(
            mutationMacro(arguments), this
          );
        }
      },
      'after', function after() {
        var parentNode = this.parentNode,
            nextSibling = this.nextSibling,
            node = mutationMacro(arguments);
        if (parentNode) {
          if (nextSibling) {
            parentNode.insertBefore(node, nextSibling);
          } else {
            parentNode.appendChild(node);
          }
        }
      },
      // WARNING - DEPRECATED - use .replaceWith() instead
      'replace', function replace() {
        this.replaceWith.apply(this, arguments);
      },
      'replaceWith', function replaceWith() {
        var parentNode = this.parentNode;
        if (parentNode) {
          parentNode.replaceChild(
            mutationMacro(arguments),
            this
          );
        }
      },
      'remove', function remove() {
        var parentNode = this.parentNode;
        if (parentNode) {
          parentNode.removeChild(this);
        }
      },
      'query', createQueryMethod('querySelector'),
      'queryAll', createQueryMethod('querySelectorAll')
    ],
    slice = properties.slice,
    i = properties.length; i; i -= 2
  ) {
    property = properties[i - 2];
    if (!(property in ElementPrototype)) {
      ElementPrototype[property] = properties[i - 1];
    }
    if (property === 'remove') {
      // see https://github.com/WebReflection/dom4/issues/19
      HTMLSelectElement.prototype[property] = function () {
        return 0 < arguments.length ?
          selectRemove.apply(this, arguments) :
          ElementPrototype.remove.call(this);
      };
    }
    // see https://github.com/WebReflection/dom4/issues/18
    if (/^(?:before|after|replace|replaceWith|remove)$/.test(property)) {
      if (CharacterData && !(property in CharacterDataPrototype)) {
        CharacterDataPrototype[property] = properties[i - 1];
      }
      if (DocumentType && !(property in DocumentTypePrototype)) {
        DocumentTypePrototype[property] = properties[i - 1];
      }
    }
    // see https://github.com/WebReflection/dom4/pull/26
    if (/^(?:append|prepend)$/.test(property)) {
      if (DocumentFragmentPrototype) {
        if (!(property in DocumentFragmentPrototype)) {
          DocumentFragmentPrototype[property] = properties[i - 1];
        }
      } else {
        try {
          createDocumentFragment().constructor.prototype[property] = properties[i - 1];
        } catch(o_O) {}
      }
    }
  }

  // bring query and queryAll to the document too
  addQueryAndAll(document);

  // brings query and queryAll to fragments as well
  if (DocumentFragmentPrototype) {
    addQueryAndAll(DocumentFragmentPrototype);
  } else {
    try {
      addQueryAndAll(createDocumentFragment().constructor.prototype);
    } catch(o_O) {}
  }

  // bring query and queryAll to the ShadowRoot too
  if (ShadowRoot) {
    addQueryAndAll(ShadowRoot.prototype);
  }

  // most likely an IE9 only issue
  // see https://github.com/WebReflection/dom4/issues/6
  if (!createElement('a').matches('a')) {
    ElementPrototype[property] = function(matches){
      return function (selector) {
        return matches.call(
          this.parentNode ?
            this :
            createDocumentFragment().appendChild(this),
          selector
        );
      };
    }(ElementPrototype[property]);
  }

  // used to fix both old webkit and SVG
  DOMTokenList.prototype = {
    length: 0,
    add: function add() {
      for(var j = 0, token; j < arguments.length; j++) {
        token = arguments[j];
        if(!this.contains(token)) {
          properties.push.call(this, property);
        }
      }
      if (this._isSVG) {
        this._.setAttribute('class', '' + this);
      } else {
        this._.className = '' + this;
      }
    },
    contains: (function(indexOf){
      return function contains(token) {
        i = indexOf.call(this, property = verifyToken(token));
        return -1 < i;
      };
    }([].indexOf || function (token) {
      i = this.length;
      while(i-- && this[i] !== token){}
      return i;
    })),
    item: function item(i) {
      return this[i] || null;
    },
    remove: function remove() {
      for(var j = 0, token; j < arguments.length; j++) {
        token = arguments[j];
        if(this.contains(token)) {
          properties.splice.call(this, i, 1);
        }
      }
      if (this._isSVG) {
        this._.setAttribute('class', '' + this);
      } else {
        this._.className = '' + this;
      }
    },
    toggle: toggle,
    toString: function toString() {
      return properties.join.call(this, SPACE);
    }
  };

  if (SVGElement && !(CLASS_LIST in SVGElement.prototype)) {
    defineProperty(SVGElement.prototype, CLASS_LIST, classListDescriptor);
  }

  // http://www.w3.org/TR/dom/#domtokenlist
  // iOS 5.1 has completely screwed this property
  // classList in ElementPrototype is false
  // but it's actually there as getter
  if (!(CLASS_LIST in document.documentElement)) {
    defineProperty(ElementPrototype, CLASS_LIST, classListDescriptor);
  } else {
    // iOS 5.1 and Nokia ASHA do not support multiple add or remove
    // trying to detect and fix that in here
    TemporaryTokenList = createElement('div')[CLASS_LIST];
    TemporaryTokenList.add('a', 'b', 'a');
    if ('a\x20b' != TemporaryTokenList) {
      // no other way to reach original methods in iOS 5.1
      TemporaryPrototype = TemporaryTokenList.constructor.prototype;
      if (!('add' in TemporaryPrototype)) {
        // ASHA double fails in here
        TemporaryPrototype = window.TemporaryTokenList.prototype;
      }
      wrapVerifyToken = function (original) {
        return function () {
          var i = 0;
          while (i < arguments.length) {
            original.call(this, arguments[i++]);
          }
        };
      };
      TemporaryPrototype.add = wrapVerifyToken(TemporaryPrototype.add);
      TemporaryPrototype.remove = wrapVerifyToken(TemporaryPrototype.remove);
      // toggle is broken too ^_^ ... let's fix it
      TemporaryPrototype.toggle = toggle;
    }
  }

  if (!('contains' in NodePrototype)) {
    defineProperty(NodePrototype, 'contains', {
      value: function (el) {
        while (el && el !== this) el = el.parentNode;
        return this === el;
      }
    });
  }

  if (!('head' in document)) {
    defineProperty(document, 'head', {
      get: function () {
        return head || (
          head = document.getElementsByTagName('head')[0]
        );
      }
    });
  }

  // requestAnimationFrame partial polyfill
  (function () {
    for (var
      raf,
      rAF = window.requestAnimationFrame,
      cAF = window.cancelAnimationFrame,
      prefixes = ['o', 'ms', 'moz', 'webkit'],
      i = prefixes.length;
      !cAF && i--;
    ) {
      rAF = rAF || window[prefixes[i] + 'RequestAnimationFrame'];
      cAF = window[prefixes[i] + 'CancelAnimationFrame'] ||
            window[prefixes[i] + 'CancelRequestAnimationFrame'];
    }
    if (!cAF) {
      // some FF apparently implemented rAF but no cAF
      if (rAF) {
        raf = rAF;
        rAF = function (callback) {
          var goOn = true;
          raf(function () {
            if (goOn) callback.apply(this, arguments);
          });
          return function () {
            goOn = false;
          };
        };
        cAF = function (id) {
          id();
        };
      } else {
        rAF = function (callback) {
          return setTimeout(callback, 15, 15);
        };
        cAF = function (id) {
          clearTimeout(id);
        };
      }
    }
    window.requestAnimationFrame = rAF;
    window.cancelAnimationFrame = cAF;
  }());

  // http://www.w3.org/TR/dom/#customevent
  try{new window.CustomEvent('?');}catch(o_O){
    window.CustomEvent = function(
      eventName,
      defaultInitDict
    ){

      // the infamous substitute
      function CustomEvent(type, eventInitDict) {
        /*jshint eqnull:true */
        var event = document.createEvent(eventName);
        if (typeof type != 'string') {
          throw new Error('An event name must be provided');
        }
        if (eventName == 'Event') {
          event.initCustomEvent = initCustomEvent;
        }
        if (eventInitDict == null) {
          eventInitDict = defaultInitDict;
        }
        event.initCustomEvent(
          type,
          eventInitDict.bubbles,
          eventInitDict.cancelable,
          eventInitDict.detail
        );
        return event;
      }

      // attached at runtime
      function initCustomEvent(
        type, bubbles, cancelable, detail
      ) {
        /*jshint validthis:true*/
        this.initEvent(type, bubbles, cancelable);
        this.detail = detail;
      }

      // that's it
      return CustomEvent;
    }(
      // is this IE9 or IE10 ?
      // where CustomEvent is there
      // but not usable as construtor ?
      window.CustomEvent ?
        // use the CustomEvent interface in such case
        'CustomEvent' : 'Event',
        // otherwise the common compatible one
      {
        bubbles: false,
        cancelable: false,
        detail: null
      }
    );
  }

  // window.Event as constructor
  try { new Event('_'); } catch (o_O) {
    /* jshint -W022 */
    o_O = (function ($Event) {
      function Event(type, init) {
        enoughArguments(arguments.length, 'Event');
        var out = document.createEvent('Event');
        if (!init) init = {};
        out.initEvent(
          type,
          !!init.bubbles,
          !!init.cancelable
        );
        return out;
      }
      Event.prototype = $Event.prototype;
      return Event;
    }(window.Event || function Event() {}));
    defineProperty(window, 'Event', {value: o_O});
    // Android 4 gotcha
    if (Event !== o_O) Event = o_O;
  }

  // window.KeyboardEvent as constructor
  try { new KeyboardEvent('_', {}); } catch (o_O) {
    /* jshint -W022 */
    o_O = (function ($KeyboardEvent) {
      // code inspired by https://gist.github.com/termi/4654819
      var
        initType = 0,
        defaults = {
          char: '',
          key: '',
          location: 0,
          ctrlKey: false,
          shiftKey: false,
          altKey: false,
          metaKey: false,
          altGraphKey: false,
          repeat: false,
          locale: navigator.language,
          detail: 0,
          bubbles: false,
          cancelable: false,
          keyCode: 0,
          charCode: 0,
          which: 0
        },
        eventType
      ;
      try {
        var e = document.createEvent('KeyboardEvent');
        e.initKeyboardEvent(
          'keyup', false, false, window, '+', 3,
          true, false, true, false, false
        );
        initType = (
          (e.keyIdentifier || e.key) == '+' &&
          (e.keyLocation || e.location) == 3
        ) && (
          e.ctrlKey ? e.altKey ? 1 : 3 : e.shiftKey ? 2 : 4
        ) || 9;
      } catch(o_O) {}
      eventType = 0 < initType ? 'KeyboardEvent' : 'Event';

      function getModifier(init) {
        for (var
          out = [],
          keys = [
            'ctrlKey',
            'Control',
            'shiftKey',
            'Shift',
            'altKey',
            'Alt',
            'metaKey',
            'Meta',
            'altGraphKey',
            'AltGraph'
          ],
          i = 0; i < keys.length; i += 2
        ) {
          if (init[keys[i]])
            out.push(keys[i + 1]);
        }
        return out.join(' ');
      }

      function withDefaults(target, source) {
        for (var key in source) {
          if (
            source.hasOwnProperty(key) &&
            !source.hasOwnProperty.call(target, key)
          ) target[key] = source[key];
        }
        return target;
      }

      function withInitValues(key, out, init) {
        try {
          out[key] = init[key];
        } catch(o_O) {}
      }

      function KeyboardEvent(type, init) {
        enoughArguments(arguments.length, 'KeyboardEvent');
        init = withDefaults(init || {}, defaults);
        var
          out = document.createEvent(eventType),
          ctrlKey = init.ctrlKey,
          shiftKey = init.shiftKey,
          altKey = init.altKey,
          metaKey = init.metaKey,
          altGraphKey = init.altGraphKey,
          modifiers = initType > 3 ? getModifier(init) : null,
          key = String(init.key),
          chr = String(init.char),
          location = init.location,
          keyCode = init.keyCode || (
            (init.keyCode = key) &&
            key.charCodeAt(0)
          ) || 0,
          charCode = init.charCode || (
            (init.charCode = chr) &&
            chr.charCodeAt(0)
          ) || 0,
          bubbles = init.bubbles,
          cancelable = init.cancelable,
          repeat = init.repeat,
          locale = init.locale,
          view = init.view || window,
          args
        ;
        if (!init.which) init.which = init.keyCode;
        if ('initKeyEvent' in out) {
          out.initKeyEvent(
            type, bubbles, cancelable, view,
            ctrlKey, altKey, shiftKey, metaKey, keyCode, charCode
          );
        } else if (0 < initType && 'initKeyboardEvent' in out) {
          args = [type, bubbles, cancelable, view];
          switch (initType) {
            case 1:
              args.push(key, location, ctrlKey, shiftKey, altKey, metaKey, altGraphKey);
              break;
            case 2:
              args.push(ctrlKey, altKey, shiftKey, metaKey, keyCode, charCode);
              break;
            case 3:
              args.push(key, location, ctrlKey, altKey, shiftKey, metaKey, altGraphKey);
              break;
            case 4:
              args.push(key, location, modifiers, repeat, locale);
              break;
            default:
              args.push(char, key, location, modifiers, repeat, locale);
          }
          out.initKeyboardEvent.apply(out, args);
        } else {
          out.initEvent(type, bubbles, cancelable);
        }
        for (key in out) {
          if (defaults.hasOwnProperty(key) && out[key] !== init[key]) {
            withInitValues(key, out, init);
          }
        }
        return out;
      }
      KeyboardEvent.prototype = $KeyboardEvent.prototype;
      return KeyboardEvent;
    }(window.KeyboardEvent || function KeyboardEvent() {}));
    defineProperty(window, 'KeyboardEvent', {value: o_O});
    // Android 4 gotcha
    if (KeyboardEvent !== o_O) KeyboardEvent = o_O;
  }

  // window.MouseEvent as constructor
  try { new MouseEvent('_', {}); } catch (o_O) {
    /* jshint -W022 */
    o_O = (function ($MouseEvent) {
      function MouseEvent(type, init) {
        enoughArguments(arguments.length, 'MouseEvent');
        var out = document.createEvent('MouseEvent');
        if (!init) init = {};
        out.initMouseEvent(
          type,
          !!init.bubbles,
          !!init.cancelable,
          init.view || window,
          init.detail || 1,
          init.screenX || 0,
          init.screenY || 0,
          init.clientX || 0,
          init.clientY || 0,
          !!init.ctrlKey,
          !!init.altKey,
          !!init.shiftKey,
          !!init.metaKey,
          init.button || 0,
          init.relatedTarget || null
        );
        return out;
      }
      MouseEvent.prototype = $MouseEvent.prototype;
      return MouseEvent;
    }(window.MouseEvent || function MouseEvent() {}));
    defineProperty(window, 'MouseEvent', {value: o_O});
    // Android 4 gotcha
    if (MouseEvent !== o_O) MouseEvent = o_O;
  }

}(window));(function (global){'use strict';

  // a WeakMap fallback for DOM nodes only used as key
  var DOMMap = global.WeakMap || (function () {

    var
      counter = 0,
      dispatched = false,
      drop = false,
      value
    ;

    function dispatch(key, ce, shouldDrop) {
      drop = shouldDrop;
      dispatched = false;
      value = undefined;
      key.dispatchEvent(ce);
    }

    function Handler(value) {
      this.value = value;
    }

    Handler.prototype.handleEvent = function handleEvent(e) {
      dispatched = true;
      if (drop) {
        e.currentTarget.removeEventListener(e.type, this, false);
      } else {
        value = this.value;
      }
    };

    function DOMMap() {
      counter++;  // make id clashing highly improbable
      this.__ce__ = new Event(('@DOMMap:' + counter) + Math.random());
    }

    DOMMap.prototype = {
      'constructor': DOMMap,
      'delete': function del(key) {
        return dispatch(key, this.__ce__, true), dispatched;
      },
      'get': function get(key) {
        dispatch(key, this.__ce__, false);
        var v = value;
        value = undefined;
        return v;
      },
      'has': function has(key) {
        return dispatch(key, this.__ce__, false), dispatched;
      },
      'set': function set(key, value) {
        dispatch(key, this.__ce__, true);
        key.addEventListener(this.__ce__.type, new Handler(value), false);
        return this;
      },
    };

    return DOMMap;

  }());

  function Dict() {}
  Dict.prototype = (Object.create || Object)(null);

  // https://dom.spec.whatwg.org/#interface-eventtarget

  function createEventListener(type, callback, options) {
    function eventListener(e) {
      if (eventListener.once) {
        e.currentTarget.removeEventListener(
          e.type,
          callback,
          eventListener
        );
        eventListener.removed = true;
      }
      if (eventListener.passive) {
        e.preventDefault = createEventListener.preventDefault;
      }
      if (typeof eventListener.callback === 'function') {
        /* jshint validthis: true */
        eventListener.callback.call(this, e);
      } else if (eventListener.callback) {
        eventListener.callback.handleEvent(e);
      }
      if (eventListener.passive) {
        delete e.preventDefault;
      }
    }
    eventListener.type = type;
    eventListener.callback = callback;
    eventListener.capture = !!options.capture;
    eventListener.passive = !!options.passive;
    eventListener.once = !!options.once;
    // currently pointless but specs say to use it, so ...
    eventListener.removed = false;
    return eventListener;
  }

  createEventListener.preventDefault = function preventDefault() {};

  var
    Event = global.CustomEvent,
    hOP = Object.prototype.hasOwnProperty,
    dE = global.dispatchEvent,
    aEL = global.addEventListener,
    rEL = global.removeEventListener,
    counter = 0,
    increment = function () { counter++; },
    indexOf = [].indexOf || function indexOf(value){
      var length = this.length;
      while(length--) {
        if (this[length] === value) {
          break;
        }
      }
      return length;
    },
    getListenerKey = function (options) {
      return ''.concat(
        options.capture ? '1' : '0',
        options.passive ? '1' : '0',
        options.once ? '1' : '0'
      );
    },
    augment, proto
  ;

  try {
    aEL('_', increment, {once: true});
    dE(new Event('_'));
    dE(new Event('_'));
    rEL('_', increment, {once: true});
  } catch(o_O) {}

  if (counter !== 1) {
    (function () {
      var dm = new DOMMap();
      function createAEL(aEL) {
        return function addEventListener(type, handler, options) {
          if (options && typeof options !== 'boolean') {
            var
              info = dm.get(this),
              key = getListenerKey(options),
              i, tmp, wrap
            ;
            if (!info) dm.set(this, (info = new Dict()));
            if (!(type in info)) info[type] = {
              handler: [],
              wrap: []
            };
            tmp = info[type];
            i = indexOf.call(tmp.handler, handler);
            if (i < 0) {
              i = tmp.handler.push(handler) - 1;
              tmp.wrap[i] = (wrap = new Dict());
            } else {
              wrap = tmp.wrap[i];
            }
            if (!(key in wrap)) {
              wrap[key] = createEventListener(type, handler, options);
              aEL.call(this, type, wrap[key], wrap[key].capture);
            }
          } else {
            aEL.call(this, type, handler, options);
          }
        };
      }
      function createREL(rEL) {
        return function removeEventListener(type, handler, options) {
          if (options && typeof options !== 'boolean') {
            var
              info = dm.get(this),
              key, i, tmp, wrap
            ;
            if (info && (type in info)) {
              tmp = info[type];
              i = indexOf.call(tmp.handler, handler);
              if (-1 < i) {
                key = getListenerKey(options);
                wrap = tmp.wrap[i];
                if (key in wrap) {
                  rEL.call(this, type, wrap[key], wrap[key].capture);
                  delete wrap[key];
                  // return if there are other wraps
                  for (key in wrap) return;
                  // otherwise remove all the things
                  tmp.handler.splice(i, 1);
                  tmp.wrap.splice(i, 1);
                  // if there are no other handlers
                  if (tmp.handler.length === 0)
                    // drop the info[type] entirely
                    delete info[type];
                }
              }
            }
          } else {
            rEL.call(this, type, handler, options);
          }
        };
      }

      augment = function (Constructor) {
        if (!Constructor) return;
        var proto = Constructor.prototype;
        proto.addEventListener = createAEL(proto.addEventListener);
        proto.removeEventListener = createREL(proto.removeEventListener);
      };

      if (global.EventTarget) {
        augment(EventTarget);
      } else {
        augment(global.Text);
        augment(global.Element || global.HTMLElement);
        augment(global.HTMLDocument);
        augment(global.Window || {prototype:global});
        augment(global.XMLHttpRequest);
      }

    }());
  }

}(self));

/*!
Copyright (C) 2014-2015 by WebReflection

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/
(function(window, document, Object, REGISTER_ELEMENT){'use strict';

// in case it's there or already patched
if (REGISTER_ELEMENT in document) return;

// DO NOT USE THIS FILE DIRECTLY, IT WON'T WORK
// THIS IS A PROJECT BASED ON A BUILD SYSTEM
// THIS FILE IS JUST WRAPPED UP RESULTING IN
// build/document-register-element.js
// and its .max.js counter part

var
  // IE < 11 only + old WebKit for attributes + feature detection
  EXPANDO_UID = '__' + REGISTER_ELEMENT + (Math.random() * 10e4 >> 0),

  // shortcuts and costants
  ATTACHED = 'attached',
  DETACHED = 'detached',
  EXTENDS = 'extends',
  ADDITION = 'ADDITION',
  MODIFICATION = 'MODIFICATION',
  REMOVAL = 'REMOVAL',
  DOM_ATTR_MODIFIED = 'DOMAttrModified',
  DOM_CONTENT_LOADED = 'DOMContentLoaded',
  DOM_SUBTREE_MODIFIED = 'DOMSubtreeModified',
  PREFIX_TAG = '<',
  PREFIX_IS = '=',

  // valid and invalid node names
  validName = /^[A-Z][A-Z0-9]*(?:-[A-Z0-9]+)+$/,
  invalidNames = [
    'ANNOTATION-XML',
    'COLOR-PROFILE',
    'FONT-FACE',
    'FONT-FACE-SRC',
    'FONT-FACE-URI',
    'FONT-FACE-FORMAT',
    'FONT-FACE-NAME',
    'MISSING-GLYPH'
  ],

  // registered types and their prototypes
  types = [],
  protos = [],

  // to query subnodes
  query = '',

  // html shortcut used to feature detect
  documentElement = document.documentElement,

  // ES5 inline helpers || basic patches
  indexOf = types.indexOf || function (v) {
    for(var i = this.length; i-- && this[i] !== v;){}
    return i;
  },

  // other helpers / shortcuts
  OP = Object.prototype,
  hOP = OP.hasOwnProperty,
  iPO = OP.isPrototypeOf,

  defineProperty = Object.defineProperty,
  gOPD = Object.getOwnPropertyDescriptor,
  gOPN = Object.getOwnPropertyNames,
  gPO = Object.getPrototypeOf,
  sPO = Object.setPrototypeOf,

  // jshint proto: true
  hasProto = !!Object.__proto__,

  // used to create unique instances
  create = Object.create || function Bridge(proto) {
    // silly broken polyfill probably ever used but short enough to work
    return proto ? ((Bridge.prototype = proto), new Bridge()) : this;
  },

  // will set the prototype if possible
  // or copy over all properties
  setPrototype = sPO || (
    hasProto ?
      function (o, p) {
        o.__proto__ = p;
        return o;
      } : (
    (gOPN && gOPD) ?
      (function(){
        function setProperties(o, p) {
          for (var
            key,
            names = gOPN(p),
            i = 0, length = names.length;
            i < length; i++
          ) {
            key = names[i];
            if (!hOP.call(o, key)) {
              defineProperty(o, key, gOPD(p, key));
            }
          }
        }
        return function (o, p) {
          do {
            setProperties(o, p);
          } while ((p = gPO(p)) && !iPO.call(p, o));
          return o;
        };
      }()) :
      function (o, p) {
        for (var key in p) {
          o[key] = p[key];
        }
        return o;
      }
  )),

  // DOM shortcuts and helpers, if any

  MutationObserver = window.MutationObserver ||
                     window.WebKitMutationObserver,

  HTMLElementPrototype = (
    window.HTMLElement ||
    window.Element ||
    window.Node
  ).prototype,

  IE8 = !iPO.call(HTMLElementPrototype, documentElement),

  isValidNode = IE8 ?
    function (node) {
      return node.nodeType === 1;
    } :
    function (node) {
      return iPO.call(HTMLElementPrototype, node);
    },

  targets = IE8 && [],

  cloneNode = HTMLElementPrototype.cloneNode,
  setAttribute = HTMLElementPrototype.setAttribute,
  removeAttribute = HTMLElementPrototype.removeAttribute,

  // replaced later on
  createElement = document.createElement,

  // shared observer for all attributes
  attributesObserver = MutationObserver && {
    attributes: true,
    characterData: true,
    attributeOldValue: true
  },

  // useful to detect only if there's no MutationObserver
  DOMAttrModified = MutationObserver || function(e) {
    doesNotSupportDOMAttrModified = false;
    documentElement.removeEventListener(
      DOM_ATTR_MODIFIED,
      DOMAttrModified
    );
  },

  // will both be used to make DOMNodeInserted asynchronous
  asapQueue,
  rAF = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (fn) { setTimeout(fn, 10); },

  // internal flags
  setListener = false,
  doesNotSupportDOMAttrModified = true,
  dropDomContentLoaded = true,

  // needed for the innerHTML helper
  notFromInnerHTMLHelper = true,

  // optionally defined later on
  onSubtreeModified,
  callDOMAttrModified,
  getAttributesMirror,
  observer,

  // based on setting prototype capability
  // will check proto or the expando attribute
  // in order to setup the node once
  patchIfNotAlready,
  patch
;

if (sPO || hasProto) {
    patchIfNotAlready = function (node, proto) {
      if (!iPO.call(proto, node)) {
        setupNode(node, proto);
      }
    };
    patch = setupNode;
} else {
    patchIfNotAlready = function (node, proto) {
      if (!node[EXPANDO_UID]) {
        node[EXPANDO_UID] = Object(true);
        setupNode(node, proto);
      }
    };
    patch = patchIfNotAlready;
}
if (IE8) {
  doesNotSupportDOMAttrModified = false;
  (function (){
    var
      descriptor = gOPD(HTMLElementPrototype, 'addEventListener'),
      addEventListener = descriptor.value,
      patchedRemoveAttribute = function (name) {
        var e = new CustomEvent(DOM_ATTR_MODIFIED, {bubbles: true});
        e.attrName = name;
        e.prevValue = this.getAttribute(name);
        e.newValue = null;
        e[REMOVAL] = e.attrChange = 2;
        removeAttribute.call(this, name);
        this.dispatchEvent(e);
      },
      patchedSetAttribute = function (name, value) {
        var
          had = this.hasAttribute(name),
          old = had && this.getAttribute(name),
          e = new CustomEvent(DOM_ATTR_MODIFIED, {bubbles: true})
        ;
        setAttribute.call(this, name, value);
        e.attrName = name;
        e.prevValue = had ? old : null;
        e.newValue = value;
        if (had) {
          e[MODIFICATION] = e.attrChange = 1;
        } else {
          e[ADDITION] = e.attrChange = 0;
        }
        this.dispatchEvent(e);
      },
      onPropertyChange = function (e) {
        // jshint eqnull:true
        var
          node = e.currentTarget,
          superSecret = node[EXPANDO_UID],
          propertyName = e.propertyName,
          event
        ;
        if (superSecret.hasOwnProperty(propertyName)) {
          superSecret = superSecret[propertyName];
          event = new CustomEvent(DOM_ATTR_MODIFIED, {bubbles: true});
          event.attrName = superSecret.name;
          event.prevValue = superSecret.value || null;
          event.newValue = (superSecret.value = node[propertyName] || null);
          if (event.prevValue == null) {
            event[ADDITION] = event.attrChange = 0;
          } else {
            event[MODIFICATION] = event.attrChange = 1;
          }
          node.dispatchEvent(event);
        }
      }
    ;
    descriptor.value = function (type, handler, capture) {
      if (
        type === DOM_ATTR_MODIFIED &&
        this.attributeChangedCallback &&
        this.setAttribute !== patchedSetAttribute
      ) {
        this[EXPANDO_UID] = {
          className: {
            name: 'class',
            value: this.className
          }
        };
        this.setAttribute = patchedSetAttribute;
        this.removeAttribute = patchedRemoveAttribute;
        addEventListener.call(this, 'propertychange', onPropertyChange);
      }
      addEventListener.call(this, type, handler, capture);
    };
    defineProperty(HTMLElementPrototype, 'addEventListener', descriptor);
  }());
} else if (!MutationObserver) {
  documentElement.addEventListener(DOM_ATTR_MODIFIED, DOMAttrModified);
  documentElement.setAttribute(EXPANDO_UID, 1);
  documentElement.removeAttribute(EXPANDO_UID);
  if (doesNotSupportDOMAttrModified) {
    onSubtreeModified = function (e) {
      var
        node = this,
        oldAttributes,
        newAttributes,
        key
      ;
      if (node === e.target) {
        oldAttributes = node[EXPANDO_UID];
        node[EXPANDO_UID] = (newAttributes = getAttributesMirror(node));
        for (key in newAttributes) {
          if (!(key in oldAttributes)) {
            // attribute was added
            return callDOMAttrModified(
              0,
              node,
              key,
              oldAttributes[key],
              newAttributes[key],
              ADDITION
            );
          } else if (newAttributes[key] !== oldAttributes[key]) {
            // attribute was changed
            return callDOMAttrModified(
              1,
              node,
              key,
              oldAttributes[key],
              newAttributes[key],
              MODIFICATION
            );
          }
        }
        // checking if it has been removed
        for (key in oldAttributes) {
          if (!(key in newAttributes)) {
            // attribute removed
            return callDOMAttrModified(
              2,
              node,
              key,
              oldAttributes[key],
              newAttributes[key],
              REMOVAL
            );
          }
        }
      }
    };
    callDOMAttrModified = function (
      attrChange,
      currentTarget,
      attrName,
      prevValue,
      newValue,
      action
    ) {
      var e = {
        attrChange: attrChange,
        currentTarget: currentTarget,
        attrName: attrName,
        prevValue: prevValue,
        newValue: newValue
      };
      e[action] = attrChange;
      onDOMAttrModified(e);
    };
    getAttributesMirror = function (node) {
      for (var
        attr, name,
        result = {},
        attributes = node.attributes,
        i = 0, length = attributes.length;
        i < length; i++
      ) {
        attr = attributes[i];
        name = attr.name;
        if (name !== 'setAttribute') {
          result[name] = attr.value;
        }
      }
      return result;
    };
  }
}

function loopAndVerify(list, action) {
  for (var i = 0, length = list.length; i < length; i++) {
    verifyAndSetupAndAction(list[i], action);
  }
}

function loopAndSetup(list) {
  for (var i = 0, length = list.length, node; i < length; i++) {
    node = list[i];
    patch(node, protos[getTypeIndex(node)]);
  }
}

function executeAction(action) {
  return function (node) {
    if (isValidNode(node)) {
      verifyAndSetupAndAction(node, action);
      loopAndVerify(
        node.querySelectorAll(query),
        action
      );
    }
  };
}

function getTypeIndex(target) {
  var
    is = target.getAttribute('is'),
    nodeName = target.nodeName.toUpperCase(),
    i = indexOf.call(
      types,
      is ?
          PREFIX_IS + is.toUpperCase() :
          PREFIX_TAG + nodeName
    )
  ;
  return is && -1 < i && !isInQSA(nodeName, is) ? -1 : i;
}

function isInQSA(name, type) {
  return -1 < query.indexOf(name + '[is="' + type + '"]');
}

function onDOMAttrModified(e) {
  var
    node = e.currentTarget,
    attrChange = e.attrChange,
    attrName = e.attrName,
    target = e.target
  ;
  if (notFromInnerHTMLHelper &&
      (!target || target === node) &&
      node.attributeChangedCallback &&
      attrName !== 'style' &&
      e.prevValue !== e.newValue) {
    node.attributeChangedCallback(
      attrName,
      attrChange === e[ADDITION] ? null : e.prevValue,
      attrChange === e[REMOVAL] ? null : e.newValue
    );
  }
}

function onDOMNode(action) {
  var executor = executeAction(action);
  return function (e) {
    asapQueue.push(executor, e.target);
  };
}

function onReadyStateChange(e) {
  if (dropDomContentLoaded) {
    dropDomContentLoaded = false;
    e.currentTarget.removeEventListener(DOM_CONTENT_LOADED, onReadyStateChange);
  }
  loopAndVerify(
    (e.target || document).querySelectorAll(query),
    e.detail === DETACHED ? DETACHED : ATTACHED
  );
  if (IE8) purge();
}

function patchedSetAttribute(name, value) {
  // jshint validthis:true
  var self = this;
  setAttribute.call(self, name, value);
  onSubtreeModified.call(self, {target: self});
}

function setupNode(node, proto) {
  setPrototype(node, proto);
  if (observer) {
    observer.observe(node, attributesObserver);
  } else {
    if (doesNotSupportDOMAttrModified) {
      node.setAttribute = patchedSetAttribute;
      node[EXPANDO_UID] = getAttributesMirror(node);
      node.addEventListener(DOM_SUBTREE_MODIFIED, onSubtreeModified);
    }
    node.addEventListener(DOM_ATTR_MODIFIED, onDOMAttrModified);
  }
  if (node.createdCallback && notFromInnerHTMLHelper) {
    node.created = true;
    node.createdCallback();
    node.created = false;
  }
}

function purge() {
  for (var
    node,
    i = 0,
    length = targets.length;
    i < length; i++
  ) {
    node = targets[i];
    if (!documentElement.contains(node)) {
      length--;
      targets.splice(i--, 1);
      verifyAndSetupAndAction(node, DETACHED);
    }
  }
}

function throwTypeError(type) {
  throw new Error('A ' + type + ' type is already registered');
}

function verifyAndSetupAndAction(node, action) {
  var
    fn,
    i = getTypeIndex(node)
  ;
  if (-1 < i) {
    patchIfNotAlready(node, protos[i]);
    i = 0;
    if (action === ATTACHED && !node[ATTACHED]) {
      node[DETACHED] = false;
      node[ATTACHED] = true;
      i = 1;
      if (IE8 && indexOf.call(targets, node) < 0) {
        targets.push(node);
      }
    } else if (action === DETACHED && !node[DETACHED]) {
      node[ATTACHED] = false;
      node[DETACHED] = true;
      i = 1;
    }
    if (i && (fn = node[action + 'Callback'])) fn.call(node);
  }
}

// set as enumerable, writable and configurable
document[REGISTER_ELEMENT] = function registerElement(type, options) {
  upperType = type.toUpperCase();
  if (!setListener) {
    // only first time document.registerElement is used
    // we need to set this listener
    // setting it by default might slow down for no reason
    setListener = true;
    if (MutationObserver) {
      observer = (function(attached, detached){
        function checkEmAll(list, callback) {
          for (var i = 0, length = list.length; i < length; callback(list[i++])){}
        }
        return new MutationObserver(function (records) {
          for (var
            current, node, newValue,
            i = 0, length = records.length; i < length; i++
          ) {
            current = records[i];
            if (current.type === 'childList') {
              checkEmAll(current.addedNodes, attached);
              checkEmAll(current.removedNodes, detached);
            } else {
              node = current.target;
              if (notFromInnerHTMLHelper &&
                  node.attributeChangedCallback &&
                  current.attributeName !== 'style') {
                newValue = node.getAttribute(current.attributeName);
                if (newValue !== current.oldValue) {
                  node.attributeChangedCallback(
                    current.attributeName,
                    current.oldValue,
                    newValue
                  );
                }
              }
            }
          }
        });
      }(executeAction(ATTACHED), executeAction(DETACHED)));
      observer.observe(
        document,
        {
          childList: true,
          subtree: true
        }
      );
    } else {
      asapQueue = [];
      rAF(function ASAP() {
        while (asapQueue.length) {
          asapQueue.shift().call(
            null, asapQueue.shift()
          );
        }
        rAF(ASAP);
      });
      document.addEventListener('DOMNodeInserted', onDOMNode(ATTACHED));
      document.addEventListener('DOMNodeRemoved', onDOMNode(DETACHED));
    }

    document.addEventListener(DOM_CONTENT_LOADED, onReadyStateChange);
    document.addEventListener('readystatechange', onReadyStateChange);

    document.createElement = function (localName, typeExtension) {
      var
        node = createElement.apply(document, arguments),
        name = '' + localName,
        i = indexOf.call(
          types,
          (typeExtension ? PREFIX_IS : PREFIX_TAG) +
          (typeExtension || name).toUpperCase()
        ),
        setup = -1 < i
      ;
      if (typeExtension) {
        node.setAttribute('is', typeExtension = typeExtension.toLowerCase());
        if (setup) {
          setup = isInQSA(name.toUpperCase(), typeExtension);
        }
      }
      notFromInnerHTMLHelper = !document.createElement.innerHTMLHelper;
      if (setup) patch(node, protos[i]);
      return node;
    };

    HTMLElementPrototype.cloneNode = function (deep) {
      var
        node = cloneNode.call(this, !!deep),
        i = getTypeIndex(node)
      ;
      if (-1 < i) patch(node, protos[i]);
      if (deep) loopAndSetup(node.querySelectorAll(query));
      return node;
    };
  }

  if (-2 < (
    indexOf.call(types, PREFIX_IS + upperType) +
    indexOf.call(types, PREFIX_TAG + upperType)
  )) {
    throwTypeError(type);
  }

  if (!validName.test(upperType) || -1 < indexOf.call(invalidNames, upperType)) {
    throw new Error('The type ' + type + ' is invalid');
  }

  var
    constructor = function () {
      return extending ?
        document.createElement(nodeName, upperType) :
        document.createElement(nodeName);
    },
    opt = options || OP,
    extending = hOP.call(opt, EXTENDS),
    nodeName = extending ? options[EXTENDS].toUpperCase() : upperType,
    upperType,
    i
  ;

  if (extending && -1 < (
    indexOf.call(types, PREFIX_TAG + nodeName)
  )) {
    throwTypeError(nodeName);
  }

  i = types.push((extending ? PREFIX_IS : PREFIX_TAG) + upperType) - 1;

  query = query.concat(
    query.length ? ',' : '',
    extending ? nodeName + '[is="' + type.toLowerCase() + '"]' : nodeName
  );

  constructor.prototype = (
    protos[i] = hOP.call(opt, 'prototype') ?
      opt.prototype :
      create(HTMLElementPrototype)
  );

  loopAndVerify(
    document.querySelectorAll(query),
    ATTACHED
  );

  return constructor;
};

}(window, document, Object, 'registerElement'));
/*jslint forin: true, plusplus: true, indent: 2, browser: true, unparam: true */
/*!
Copyright (C) 2014-2015 by Andrea Giammarchi - @WebReflection

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/
var restyle = (function (O) {
  'use strict';

  var
    toString = O.toString,
    has = O.hasOwnProperty,
    camelFind = /([a-z])([A-Z])/g,
    ignoreSpecial = /^@(?:page|font-face)/,
    isMedia = /^@(?:media)/,
    isArray = Array.isArray || function (arr) {
      return toString.call(arr) === '[object Array]';
    },
    empty = [],
    restyle;

  function ReStyle(component, node, css, prefixes, doc) {
    this.component = component;
    this.node = node;
    this.css = css;
    this.prefixes = prefixes;
    this.doc = doc;
  }

  function replace(substitute) {
    if (!(substitute instanceof ReStyle)) {
      substitute = restyle(
        this.component, substitute, this.prefixes, this.doc
      );
    }
    this.remove();
    ReStyle.call(
      this,
      substitute.component,
      substitute.node,
      substitute.css,
      substitute.prefixes,
      substitute.doc
    );
  }

  ReStyle.prototype = {
    overwrite: replace,
    replace: replace,
    set: replace,
    remove: function () {
      var node = this.node,
        parentNode = node.parentNode;
      if (parentNode) {
        parentNode.removeChild(node);
      }
    },
    valueOf: function () {
      return this.css;
    }
  };

  function camelReplace(m, $1, $2) {
    return $1 + '-' + $2.toLowerCase();
  }

  function create(key, value, prefixes) {
    var
      css = [],
      pixels = typeof value === 'number' ? 'px' : '',
      k = key.replace(camelFind, camelReplace),
      i;
    for (i = 0; i < prefixes.length; i++) {
      css.push('-', prefixes[i], '-', k, ':', value, pixels, ';');
    }
    css.push(k, ':', value, pixels, ';');
    return css.join('');
  }

  function property(previous, key) {
    return previous.length ? previous + '-' + key : key;
  }

  function generate(css, previous, obj, prefixes) {
    var key, value, i;
    for (key in obj) {
      if (has.call(obj, key)) {
        if (typeof obj[key] === 'object') {
          if (isArray(obj[key])) {
            value = obj[key];
            for (i = 0; i < value.length; i++) {
              css.push(
                create(property(previous, key), value[i], prefixes)
              );
            }
          } else {
            generate(
              css,
              property(previous, key),
              obj[key],
              prefixes
            );
          }
        } else {
          css.push(
            create(property(previous, key), obj[key], prefixes)
          );
        }
      }
    }
    return css.join('');
  }

  function parse(component, obj, prefixes) {
    var
      css = [],
      at, cmp, special, k, v,
      same, key, value, i, j;
    for (key in obj) {
      if (has.call(obj, key)) {
        j = key.length;
        if (!j) key = component.slice(0, -1);
        at = key.charAt(0) === '@';
        same = at || !component.indexOf(key + ' ');
        cmp = at && isMedia.test(key) ? component : '';
        special = at && !ignoreSpecial.test(key);
        k = special ? key.slice(1) : key;
        value = empty.concat(obj[j ? key : '']);
        for (i = 0; i < value.length; i++) {
          v = value[i];
          if (special) {
            j = prefixes.length;
            while (j--) {
              css.push('@-', prefixes[j], '-', k, '{',
                parse(cmp, v, [prefixes[j]]),
                '}');
            }
            css.push(key, '{', parse(cmp, v, prefixes), '}');
          } else {
            css.push(
              same ? key : component + key,
              '{', generate([], '', v, prefixes), '}'
            );
          }
        }
      }
    }
    return css.join('');
  }

  // hack to avoid JSLint shenanigans
  if ({undefined: true}[typeof document]) {
    // in node, by default, no prefixes are used
    restyle = function (component, obj, prefixes) {
      if (typeof component === 'object') {
        prefixes = obj;
        obj = component;
        component = '';
      } else {
        component += ' ';
      }
      return parse(component, obj, prefixes || empty);
    };
    // useful for different style of require
    restyle.restyle = restyle;
  } else {
    restyle = function (component, obj, prefixes, doc) {
      if (typeof component === 'object') {
        doc = prefixes;
        prefixes = obj;
        obj = component;
        c = (component = '');
      } else {
        c = component + ' ';
      }
      var c, d = doc || (doc = document),
        css = parse(c, obj, prefixes || (prefixes = restyle.prefixes)),
        head = d.head ||
          d.getElementsByTagName('head')[0] ||
          d.documentElement,
        node = head.insertBefore(
          d.createElement('style'),
          head.lastChild
        );
      node.type = 'text/css';
      // it should have been
      // if ('styleSheet' in node) {}
      // but JSLint bothers in that way
      if (node.styleSheet) {
        node.styleSheet.cssText = css;
      } else {
        node.appendChild(d.createTextNode(css));
      }
      return new ReStyle(component, node, css, prefixes, doc);
    };
  }

  // bringing animation utility in window-aware world only
  if (!{undefined: true}[typeof window]) {
    restyle.animate = (function (g) {

      var
        rAF = window.requestAnimationFrame ||
              window.webkitRequestAnimationFrame ||
              window.mozRequestAnimationFrame ||
              window.msRequestAnimationFrame ||
              function (fn) { setTimeout(fn, 10); },
        liveStyles = {},
        uid = 'restyle-'.concat(Math.random() * (+new Date()), '-'),
        uidIndex = 0,
        animationType,
        transitionType
      ;

      switch (true) {
        case !!g.AnimationEvent:
          animationType = 'animationend';
          break;
        case !!g.WebKitAnimationEvent:
          animationType = 'webkitAnimationEnd';
          break;
        case !!g.MSAnimationEvent:
          animationType = 'MSAnimationEnd';
          break;
        case !!g.OAnimationEvent:
          animationType = 'oanimationend';
          break;
      }

      switch (true) {
        case !!g.TransitionEvent:
          transitionType = 'transitionend';
          break;
        case !!g.WebKitTransitionEvent:
          transitionType = 'webkitTransitionEnd';
          break;
        case !!g.MSTransitionEvent:
          transitionType = 'MSTransitionEnd';
          break;
        case !!g.OTransitionEvent:
          transitionType = 'oTransitionEnd';
          break;
      }

      restyle.transition = function (el, info, callback) {
        var
          transition = info.transition || 'all .3s ease-out',
          id = el.getAttribute('id'),
          to = [].concat(info.to),
          from = update({}, info.from),
          noID = !id,
          style = {},
          currentID,
          result,
          live,
          t
        ;
        function drop() {
          if (transitionType) {
            el.removeEventListener(transitionType, onTransitionEnd, false);
          } else {
            clearTimeout(t);
            t = 0;
          }
        }
        function next() {
          style[currentID] = (live.last = update(from, to.shift()));
          live.css.replace(style);
          if (transitionType) {
            el.addEventListener(transitionType, onTransitionEnd, false);
          } else {
            t = setTimeout(onTransitionEnd, 10);
          }
        }
        function onTransitionEnd(e) {
          drop();
          if (to.length) {
            rAF(next);
          } else {
            if (!e) e = new CustomEvent('transitionend', {detail: result});
            else e.detail = result;
            if (callback) callback.call(el, e);
          }
        }
        function update(target, source) {
          for (var k in source) target[k] = source[k];
          return target;
        }
        if (noID) el.setAttribute('id', id = (uid + uidIndex++).replace('.','-'));
        currentID = '#' + id;
        if (liveStyles.hasOwnProperty(id)) {
          live = liveStyles[id];
          from = (live.last = update(live.last, from));
          style[currentID] = from;
          live.transition.remove();
          live.css.replace(style);
        } else {
          live = liveStyles[id] = {
            last: (style[currentID] = from),
            css: restyle(style)
          };
        }
        rAF(function() {
          style[currentID] = {transition: transition};
          live.transition = restyle(style);
          rAF(next);
        });
        return (result = {
          clean: function () {
            if (noID) el.removeAttribute('id');
            drop();
            live.transition.remove();
            live.css.remove();
            delete liveStyles[id];
          },
          drop: drop
        });
      };

      ReStyle.prototype.getAnimationDuration = function (el, name) {
        for (var
          chunk, duration,
          classes = el.className.split(/\s+/),
          i = classes.length; i--;
        ) {
          chunk = classes[i];
          if (
            chunk.length &&
            (new RegExp('\\.' + chunk + '(?:|\\{|\\,)([^}]+?)\\}')).test(this.css)
          ) {
            chunk = RegExp.$1;
            if (
              (new RegExp(
                'animation-name:' +
                name +
                ';.*?animation-duration:([^;]+?);'
              )).test(chunk) ||
              (new RegExp(
                'animation:\\s*' + name + '\\s+([^\\s]+?);'
              )).test(chunk)
            ) {
              chunk = RegExp.$1;
              duration = parseFloat(chunk);
              if (duration) {
                return duration * (/[^m]s$/.test(chunk) ? 1000 : 1);
              }
            }
          }
        }
        return -1;
      };

      ReStyle.prototype.getTransitionDuration = function (el) {
        var
          cs = getComputedStyle(el),
          duration = cs.getPropertyValue('transition-duration') ||
                     /\s(\d+(?:ms|s))/.test(
                       cs.getPropertyValue('transition')
                     ) && RegExp.$1
        ;
        return parseFloat(duration) * (/[^m]s$/.test(duration) ? 1000 : 1);
      };

      ReStyle.prototype.transit = transitionType ?
        function (el, callback) {
          function onTransitionEnd(e) {
            drop();
            callback.call(el, e);
          }
          function drop() {
            el.removeEventListener(transitionType, onTransitionEnd, false);
          }
          el.addEventListener(transitionType, onTransitionEnd, false);
          return {drop: drop};
        } :
        function (el, callback) {
          var i = setTimeout(callback, this.getTransitionDuration(el));
          return {drop: function () {
            clearTimeout(i);
          }};
        }
      ;

      ReStyle.prototype.animate = animationType ?
        function animate(el, name, callback) {
          function onAnimationEnd(e) {
            if (e.animationName === name) {
              drop();
              callback.call(el, e);
            }
          }
          function drop() {
            el.removeEventListener(animationType, onAnimationEnd, false);
          }
          el.addEventListener(animationType, onAnimationEnd, false);
          return {drop: drop};
        } :
        function animate(el, name, callback) {
          var i, drop, duration = this.getAnimationDuration(el, name);
          if (duration < 0) {
            drop = O;
          } else {
            i = setTimeout(
              function () {
                callback.call(el, {
                  type: 'animationend',
                  animationName: name,
                  currentTarget: el,
                  target: el,
                  stopImmediatePropagation: O,
                  stopPropagation: O,
                  preventDefault: O
                });
              },
              duration
            );
            drop = function () {
              clearTimeout(i);
            };
          }
          return {drop: drop};
        }
      ;
    }(window));
  }

  restyle.customElement = function (name, constructor, proto) {
    var
      key,
      ext = 'extends',
      prototype = Object.create(constructor.prototype),
      descriptor = {prototype: prototype},
      has = descriptor.hasOwnProperty,
      isExtending = proto && has.call(proto, ext)
    ;
    if (isExtending) {
      descriptor[ext] = proto[ext];
    }
    for (key in proto) {
      if (key !== ext) {
        prototype[key] = (
          key === 'css' ?
            restyle(
              isExtending ?
               (proto[ext] + '[is=' + name + ']') :
               name,
              proto[key]
            ) :
            proto[key]
        );
      }
    }
    return document.registerElement(name, descriptor);
  };

  restyle.prefixes = [
    'webkit',
    'moz',
    'ms',
    'o'
  ];

  return restyle;

/**
 * not sure if TODO since this might be prependend regardless the parser
 *  @namespace url(http://www.w3.org/1999/xhtml);
 *  @charset "UTF-8";
 */

}({}));
/*!
Copyright (C) 2015 by Andrea Giammarchi - @WebReflection

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/
var Class = Class || (function (Object) {
  'use strict';

  /*! (C) Andrea Giammarchi - MIT Style License */

  var
    // shortcuts for minifiers and ES3 private keywords too
    CONSTRUCTOR = 'constructor',
    EXTENDS = 'extends',
    IMPLEMENTS = 'implements',
    INIT = 'init',
    PROTOTYPE = 'prototype',
    STATIC = 'static',
    SUPER = 'super',
    TO_STRING = 'toString',
    VALUE = 'value',
    WITH = 'with',

    // infamous property used as fallback
    // for IE8 and lower only
    PROTO = '__proto__',

    // used to copy non enumerable properties on IE
    nonEnumerables = [
      'hasOwnProperty',
      'isPrototypeOf',
      'propertyIsEnumerable',
      'toLocaleString',
      TO_STRING,
      'valueOf'
    ],

    // common shortcuts
    ObjectPrototype = Object[PROTOTYPE],
    hOP = ObjectPrototype[nonEnumerables[0]],
    toString = ObjectPrototype[TO_STRING],

    // Espruino 1.7x does not have (yet) Object.prototype.propertyIsEnumerable
    propertyIsEnumerable = ObjectPrototype[nonEnumerables[2]] || function (p) {
      for (var k in this) if (p === k) return hOP.call(this, p);
      return false;
    },

    // IE < 9 bug only
    hasIEEnumerableBug = !propertyIsEnumerable.call({toString: 0}, TO_STRING),

    // basic ad-hoc private fallback for old browsers
    // use es5-shim if you want a properly patched polyfill
    create = Object.create || function (proto) {
      /*jshint newcap: false */
      var isInstance = this instanceof create;
      create[PROTOTYPE] = isInstance ? createPrototype : (proto || ObjectPrototype);
      return isInstance ? this : new create();
    },

    // very old browsers actually work better
    // without assigning null as prototype
    createPrototype = create[PROTOTYPE],

    // redefined if not present
    defineProperty = Object.defineProperty,

    // redefined if not present
    gOPD = Object.getOwnPropertyDescriptor,

    // basic ad-hoc private fallback for old browsers
    // use es5-shim if you want a properly patched polyfill
    gOPN = Object.getOwnPropertyNames || function (object) {
        var names = [], i, key;
        for (key in object) {
          if (hOP.call(object, key)) {
            names.push(key);
          }
        }
        if (hasIEEnumerableBug) {
          for (i = 0; i < nonEnumerables.length; i++) {
            key = nonEnumerables[i];
            if (hOP.call(object, key)) {
              names.push(key);
            }
          }
        }
        return names;
    },

    // basic ad-hoc private fallback for old browsers
    // returns empty Array if nonexistent
    gOPS = Object.getOwnPropertySymbols || function () {
      return [];
    },

    // needed to verify the existence
    getPrototypeOf = Object.getPrototypeOf,

    // needed to allow Classes as traits
    gPO = getPrototypeOf || function (o) {
      return o[PROTO] || null;
    },

    // equivalent of Reflect.ownKeys
    oK = function (o) {
      return gOPN(o).concat(gOPS(o));
    },

    // used to filter mixin  Symbol
    isArray = Array.isArray || function (a) {
      return toString.call(a) === '[object Array]';
    },

    // used to avoid setting `arguments` and other function properties
    // when public static are copied over
    nativeFunctionOPN = gOPN(function () {}).concat('arguments'),
    indexOf = nativeFunctionOPN.indexOf || function (v) {
      for (var i = this.length; i-- && this[i] !== v;) {}
      return i;
    },

    // used to flag classes
    isClassDescriptor = {value: true},

    trustSuper = ('' + function () {
      // this test should never be minifier sensitive
      // or the indexOf check after will fail
      this['super']();
    }).indexOf(SUPER) < 0 ?
      // In 2010 Opera 10.5 for Linux Debian 6
      // goes nut with methods to string representation,
      // truncating pieces of text in an unpredictable way.
      // If you are targeting such browser
      // be aware that super invocation might fail.
      // This is the only exception I could find
      // from year 2000 to modern days browsers
      // plus everything else would work just fine.
      function () { return true; } :
      // all other JS engines should be just fine
      function (method) {
        var
          str = '' + method,
          i = str.indexOf(SUPER)
        ;
        return i < 0 ?
          false :
          isBoundary(str.charCodeAt(i - 1)) &&
          isBoundary(str.charCodeAt(i + 5));
      }
  ;

  // verified broken IE8 or older browsers
  try {
    defineProperty({}, '{}', {});
  } catch(o_O) {
    if ('__defineGetter__' in {}) {
      defineProperty = function (object, name, descriptor) {
        if (hOP.call(descriptor, VALUE)) {
          object[name] = descriptor[VALUE];
        } else {
          if (hOP.call(descriptor, 'get')) {
            object.__defineGetter__(name, descriptor.get);
          }
          if (hOP.call(descriptor, 'set')) {
            object.__defineSetter__(name, descriptor.set);
          }
        }
        return object;
      };
      gOPD = function (object, key) {
        var
          get = object.__lookupGetter__(key),
          set = object.__lookupSetter__(key),
          descriptor = {}
        ;
        if (get || set) {
          if (get) {
            descriptor.get = get;
          }
          if (set) {
            descriptor.set = set;
          }
        } else {
          descriptor[VALUE] = object[key];
        }
        return descriptor;
      };
    } else {
      defineProperty = function (object, name, descriptor) {
        object[name] = descriptor[VALUE];
        return object;
      };
      gOPD = function (object, key) {
        return {value: object[key]};
      };
    }
  }

  // copy all imported enumerable methods and properties
  function addMixins(mixins, target, inherits, isNOTExtendingNative) {
    for (var
      source,
      init = [],
      i = 0; i < mixins.length; i++
    ) {
      source = transformMixin(mixins[i]);
      if (hOP.call(source, INIT)) {
        init.push(source[INIT]);
      }
      copyOwn(source, target, inherits, false, false, isNOTExtendingNative);
    }
    return init;
  }

  // deep copy all properties of an object (static objects only)
  function copyDeep(source) {
    for (var
      key, descriptor, value,
      target = create(gPO(source)),
      names = oK(source),
      i = 0; i < names.length; i++
    ) {
      key = names[i];
      descriptor = gOPD(source, key);
      if (hOP.call(descriptor, VALUE)) {
        copyValueIfObject(descriptor, copyDeep);
      }
      defineProperty(target, key, descriptor);
    }
    return target;
  }

  // given two objects, performs a deep copy
  // per each property not present in the target
  // otherwise merges, without overwriting,
  // all properties within the object
  function copyMerged(source, target) {
    for (var
      key, descriptor, value, tvalue,
      names = oK(source),
      i = 0; i < names.length; i++
    ) {
      key = names[i];
      descriptor = gOPD(source, key);
      // target already has this property
      if (hOP.call(target, key)) {
        // verify the descriptor can  be merged
        if (hOP.call(descriptor, VALUE)) {
          value = descriptor[VALUE];
          // which means, verify it's an object
          if (isObject(value)) {
            // in such case, verify the target can be modified
            descriptor = gOPD(target, key);
            // meaning verify it's a data descriptor
            if (hOP.call(descriptor, VALUE)) {
              tvalue = descriptor[VALUE];
              // and it's actually an object
              if (isObject(tvalue)) {
                copyMerged(value, tvalue);
              }
            }
          }
        }
      } else {
        // target has no property at all
        if (hOP.call(descriptor, VALUE)) {
          // copy deep if it's an object
          copyValueIfObject(descriptor, copyDeep);
        }
        defineProperty(target, key, descriptor);
      }
    }
  }

  // configure source own properties in the target
  function copyOwn(source, target, inherits, publicStatic, allowInit, isNOTExtendingNative) {
    for (var
      key,
      noFunctionCheck = typeof source !== 'function',
      names = oK(source),
      i = 0; i < names.length; i++
    ) {
      key = names[i];
      if (
        (noFunctionCheck || indexOf.call(nativeFunctionOPN, key) < 0) &&
        isNotASpecialKey(key, allowInit)
      ) {
        if (hOP.call(target, key)) {
          warn('duplicated: ' + key.toString());
        }
        setProperty(inherits, target, key, gOPD(source, key), publicStatic, isNOTExtendingNative);
      }
    }
  }

  // shortcut to copy objects into descriptor.value
  function copyValueIfObject(where, how) {
    var what = where[VALUE];
    if (isObject(what)) {
      where[VALUE] = how(what);
    }
  }


  // return the right constructor analyzing the parent.
  // if the parent is empty there is no need to call it.
  function createConstructor(hasParentPrototype, parent) {
    var Class = function Class() {};
    return hasParentPrototype && ('' + parent) !== ('' + Class) ?
      function Class() {
        return parent.apply(this, arguments);
      } :
      Class
    ;
  }

  // common defineProperty wrapper
  function define(target, key, value, publicStatic) {
    var configurable = isConfigurable(key, publicStatic);
    defineProperty(target, key, {
      enumerable: false, // was: publicStatic,
      configurable: configurable,
      writable: configurable,
      value: value
    });
  }

  // verifies a specific char code is not in [A-Za-z_]
  // used to avoid RegExp for non RegExp aware environment
  function isBoundary(code) {
    return code ?
      (code < 65 || 90 < code) &&
      (code < 97 || 122 < code) &&
      code !== 95 :
      true;
  }

  // if key is UPPER_CASE and the property is public static
  // it will define the property as non configurable and non writable
  function isConfigurable(key, publicStatic) {
    return publicStatic ? !isPublicStatic(key) : true;
  }

  // verifies a key is not special for the class
  function isNotASpecialKey(key, allowInit) {
    return  key !== CONSTRUCTOR &&
            key !== EXTENDS &&
            key !== IMPLEMENTS &&
            // Blackberry 7 and old WebKit bug only:
            //  user defined functions have
            //  enumerable prototype and constructor
            key !== PROTOTYPE &&
            key !== STATIC &&
            key !== SUPER &&
            key !== WITH &&
            (allowInit || key !== INIT);
  }

  // verifies a generic value is actually an object
  function isObject(value) {
    /*jshint eqnull: true */
    return value != null && typeof value === 'object';
  }

  // verifies the entire string is upper case
  // and contains eventually an underscore
  // used to avoid RegExp for non RegExp aware environment
  function isPublicStatic(key) {
    for(var c, i = 0; i < key.length; i++) {
      c = key.charCodeAt(i);
      if ((c < 65 || 90 < c) && c !== 95) {
        return false;
      }
    }
    return true;
  }

  // will eventually convert classes or constructors
  // into trait objects, before assigning them as such
  function transformMixin(trait) {
    if (isObject(trait)) return trait;
    else {
      var i, key, keys, object, proto;
      if (trait.isClass) {
        if (trait.length) {
          warn((trait.name || 'Class') + ' should not expect arguments');
        }
        for (
          object = {init: trait},
          proto = trait.prototype;
          proto && proto !== Object.prototype;
          proto = gPO(proto)
        ) {
          for (i = 0, keys = oK(proto); i < keys.length; i++) {
            key = keys[i];
            if (isNotASpecialKey(key, false) && !hOP.call(object, key)) {
              defineProperty(object, key, gOPD(proto, key));
            }
          }
        }
      } else {
        for (
          i = 0,
          object = {},
          proto = trait({}),
          keys = oK(proto);
          i < keys.length; i++
        ) {
          key = keys[i];
          if (key !== INIT) {
            // if this key is the mixin one
            if (~key.toString().indexOf('mixin:init') && isArray(proto[key])) {
              // set the init simply as own method
              object.init = proto[key][0];
            } else {
              // simply assign the descriptor
              defineProperty(object, key, gOPD(proto, key));
            }
          }
        }
      }
      return object;
    }
  }

  // set a property via defineProperty using a common descriptor
  // only if properties where not defined yet.
  // If publicStatic is true, properties are both non configurable and non writable
  function setProperty(inherits, target, key, descriptor, publicStatic, isNOTExtendingNative) {
    var
      hasValue = hOP.call(descriptor, VALUE),
      configurable,
      value
    ;
    if (publicStatic) {
      if (hOP.call(target, key)) {
        // in case the value is not a static one
        if (
          inherits &&
          isObject(target[key]) &&
          isObject(inherits[CONSTRUCTOR][key])
        ) {
          copyMerged(inherits[CONSTRUCTOR][key], target[key]);
        }
        return;
      } else if (hasValue) {
        // in case it's an object perform a deep copy
        copyValueIfObject(descriptor, copyDeep);
      }
    } else if (hasValue) {
      value = descriptor[VALUE];
      if (typeof value === 'function' && trustSuper(value)) {
        descriptor[VALUE] = wrap(inherits, key, value, publicStatic);
      }
    } else if (isNOTExtendingNative) {
      wrapGetOrSet(inherits, key, descriptor, 'get');
      wrapGetOrSet(inherits, key, descriptor, 'set');
    }
    configurable = isConfigurable(key, publicStatic);
    descriptor.enumerable = false; // was: publicStatic;
    descriptor.configurable = configurable;
    if (hasValue) {
      descriptor.writable = configurable;
    }
    defineProperty(target, key, descriptor);
  }

  // basic check against expected properties or methods
  // used when `implements` is used
  function verifyImplementations(interfaces, target) {
    for (var
      current,
      key,
      i = 0; i < interfaces.length; i++
    ) {
      current = interfaces[i];
      for (key in current) {
        if (hOP.call(current, key) && !hOP.call(target, key)) {
          warn(key.toString() + ' is not implemented');
        }
      }
    }
  }

  // warn if something doesn't look right
  // such overwritten public statics
  // or traits / mixins assigning twice same thing
  function warn(message) {
    try {
      console.warn(message);
    } catch(meh) {
      /*\_(ツ)_*/
    }
  }

  // lightweight wrapper for methods that requires
  // .super(...) invokaction - inspired by old klass.js
  function wrap(inherits, key, method, publicStatic) {
    return function () {
      if (!hOP.call(this, SUPER)) {
        // define it once in order to use
        // fast assignment every other time
        define(this, SUPER, null, publicStatic);
      }
      var
        previous = this[SUPER],
        current = (this[SUPER] = inherits[key]),
        result = method.apply(this, arguments)
      ;
      this[SUPER] = previous;
      return result;
    };
  }

  // get/set shortcut for the eventual wrapper
  function wrapGetOrSet(inherits, key, descriptor, gs, publicStatic) {
    if (hOP.call(descriptor, gs) && trustSuper(descriptor[gs])) {
      descriptor[gs] = wrap(
        gOPD(inherits, key),
        gs,
        descriptor[gs],
        publicStatic
      );
    }
  }

  // the actual Class({ ... }) definition
  return function (description) {
    var
      hasConstructor = hOP.call(description, CONSTRUCTOR),
      hasParent = hOP.call(description, EXTENDS),
      parent = hasParent && description[EXTENDS],
      hasParentPrototype = hasParent && typeof parent === 'function',
      inherits = hasParentPrototype ? parent[PROTOTYPE] : parent,
      constructor = hasConstructor ?
        description[CONSTRUCTOR] :
        createConstructor(hasParentPrototype, parent),
      hasSuper = hasParent && hasConstructor && trustSuper(constructor),
      prototype = hasParent ? create(inherits) : constructor[PROTOTYPE],
      // do not wrap getters and setters in GJS extends
      isNOTExtendingNative = toString.call(inherits).indexOf(' GObject_') < 0,
      mixins,
      length
    ;
    if (hasSuper && isNOTExtendingNative) {
      constructor = wrap(inherits, CONSTRUCTOR, constructor, false);
    }
    // add modules/mixins (that might swap the constructor)
    if (hOP.call(description, WITH)) {
      mixins = addMixins([].concat(description[WITH]), prototype, inherits, isNOTExtendingNative);
      length = mixins.length;
      if (length) {
        constructor = (function (parent) {
          return function () {
            var i = 0;
            while (i < length) mixins[i++].call(this);
            return parent.apply(this, arguments);
          };
        }(constructor));
        constructor[PROTOTYPE] = prototype;
      }
    }
    if (hOP.call(description, STATIC)) {
      // add new public static properties first
      copyOwn(description[STATIC], constructor, inherits, true, true, isNOTExtendingNative);
    }
    if (hasParent) {
      // in case it's a function
      if (parent !== inherits) {
        // copy possibly inherited statics too
        copyOwn(parent, constructor, inherits, true, true, isNOTExtendingNative);
      }
      constructor[PROTOTYPE] = prototype;
    }
    if (prototype[CONSTRUCTOR] !== constructor) {
      define(prototype, CONSTRUCTOR, constructor, false);
    }
    // enrich the prototype
    copyOwn(description, prototype, inherits, false, true, isNOTExtendingNative);
    if (hOP.call(description, IMPLEMENTS)) {
      verifyImplementations([].concat(description[IMPLEMENTS]), prototype);
    }
    if (hasParent && !getPrototypeOf) {
      define(prototype, PROTO, inherits, false);
    }
    return defineProperty(constructor, 'isClass', isClassDescriptor);
  };

}(Object));
Object.defineProperty(DOMClass, 'bindings', {
  enumerable: true,
  value: (function (O) {'use strict';

    /*! (C) 2015 Andrea Giammarchi - Mit Style License */

    // I must admit this wasn't fun ... so many quirks!
    // works down to Android 2.3, webOS (wut?!) and Windows Phone 7
    // BB7 has problems defining properties on DOM Object
    // ... I salute you BB7, it's been fun!

    var
      // constants
      STATE_OFF = 0,
      STATE_DIRECT = 1,
      STATE_ATTRIBUTE = 2,
      STATE_EVENT = 4,
      DOM_ATTR_MODIFIED = 'DOMAttrModified',
      ON_ATTACHED = 'attachedCallback',
      ON_DETACHED = 'detachedCallback',
      GET_ATTRIBUTE = 'getAttribute',
      SET_ATTRIBUTE = 'setAttribute',
      DROP_BINDINGS = 'destroyBindings',
      // shortcuts
      create = O.create,
      dP = O.defineProperty,
      gPO = O.getPrototypeOf,
      gOPD = O.getOwnPropertyDescriptor,
      // RegExp used all the time
      ignore = /IFRAME|NOFRAMES|NOSCRIPT|SCRIPT|SELECT|STYLE|TEXTAREA|[a-z]/,

      // changed {} to []
      //oneWay = /\{\{\{?[\S\s]+?\}\}\}?/g,
      oneWay = /\[\[\[?[\S\s]+?\]\]\]?/g,
      //oneWayHTML = /^\{[\S\s]+?\}$/,
      oneWayHTML = /^\[[\S\s]+?\]$/,
      comma = /\s*,\s*/,
      colon = /\s*:\s*/,
      spaces = /^\s+|\s+$/g,
      decepticons = /^([\S]+?)\(([\S\s]*?)\)/,
      // MutationObserver common options
      whatToObserve = {attributes: true, subtree: true},
      // used to create HTML
      dummy = document.createElement('dummy'),
      // moar shortcuts
      hOP = whatToObserve.hasOwnProperty,
      notNull = function (v, f) {
        /* jshint eqnull:true */
        return v == null ? f : v;
      },
      on = function (el, type, handler) {
        el.addEventListener(type, handler, true);
        //                                  ^ ... but why?
        // because usually devs set listeners in bubbling
        // phase, meaning this one might fire before ^_^
      },
      off = function (el, type, handler) {
        el.removeEventListener(type, handler, true);
      },
      trim = DOM_ATTR_MODIFIED.trim || function () {
        return this.replace(spaces, '');
      },
      createGetSet = function (get, set) {
        return {
          configurable: true,
          enumerable: true,
          get: get,
          set: set
        };
      },
      // if attributes such onchange or others are used
      directMethod = function (bindings, method) {
        return typeof method === 'function' ?
          function () { return method.apply(bindings, arguments); } :
          method;
      },
      // given an element and a property
      // with direct info manipulation capability
      // test if there is a descriptor that could be
      // used in case it's wrapped/redefined
      getInterceptor = function (el, key) {
        var proto = el, descriptor;
        while (proto && !hOP.call(proto, key)) proto = gPO(proto);
        if (proto) {
          descriptor = gOPD(proto, key);
          if ('set' in descriptor && 'get' in descriptor) {
            try {
              if (descriptor.get.call(el) !== el[key]) {
                throw descriptor;
              }
              return descriptor;
            } catch (iOS) {}
          }
        }
        return null;
      },
      MO =  window.MutationObserver ||
            window.WebKitMutationObserver ||
            window.MozMutationObserver,
      hasMo = !!MO,
      schedule = function (fn) {
        // requestAnimationFrame would be too greedy
        // however, it has its advantages like not bothering
        // when the window/tab is not focused or something else.
        // in this way we are sure we'll reschedule the timer
        // only if rAF executes, instead of forever re-scheduled
        return setTimeout(requestAnimationFrame, 100, fn);
      },
      cancel = clearTimeout, // cancelAnimationFrame
      hasDAM = hasMo
    ;

    // verify if the DOMAttrModified listener works at all
    if (!hasDAM) {
      (function verifyItWorks(html, uid) {
        function suchAGoodBoy() { hasDAM = true; }
        on(html, DOM_ATTR_MODIFIED, suchAGoodBoy);
        html[SET_ATTRIBUTE](uid, 1);
        html.removeAttribute(uid);
        off(html, DOM_ATTR_MODIFIED, suchAGoodBoy);
      }(
        document.documentElement,
        'dom-' + (Math.random() + '-class').slice(2)
      ));
    }

    // if there's some textual binding in the wild, like inside
    // a generic node, it will be bound "one way" here
    function boundTextNode(bindings, key, node, dB, dS) {
      var value, setter = hOP.call(bindings, key) && gOPD(bindings, key).set;
      dP(bindings, key, createGetSet(
        function get() { return value; },
        function set(v) {
          node.nodeValue = (value = v);
          if (dB) dS(key);
          if (setter) setter.call(bindings, v);
        }
      ));
      return node;
    }

    // if there's some HTML binding in the wild, like inside
    // a generic node, it will be bound "one way" here
    function boundFragmentNode(bindings, key, document, innerHTML, dB, dS) {
      var
        setter = hOP.call(bindings, key) && gOPD(bindings, key).set,
        pins = createFragment(document, innerHTML)
      ;
      dP(bindings, key, createGetSet(
        function get() { return innerHTML; },
        function set(value) {
          pins = updatePins(document, pins, (innerHTML = value));
          if (dB) dS(key);
          if (setter) setter.call(bindings, value);
        }
      ));
      return pins.fragment;
    }

    // create a documentFragment adding before and after
    // two comments node. There are immune to styles
    // but these are also nodes so it's possible later on
    // to replace the fragment within these comments ;-)
    function createFragment(document, innerHTML) {
      var pins, firstChild;
      dummy.innerHTML = '<!---->' + innerHTML + '<!---->';
      pins = {
        start: dummy.firstChild,
        end: dummy.lastChild,
        fragment: document.createDocumentFragment()
      };
      while ((firstChild = dummy.firstChild))
        pins.fragment.appendChild(firstChild);
      return pins;
    }

    // used to replace previous fragment
    // will create a new "pins" object with
    // comments boundaries and the fragment
    function updatePins(document, pins, html) {
      var
        start = pins.start,
        parentNode = start.parentNode,
        nextSibling
      ;
      do {
        nextSibling = start.nextSibling;
        parentNode.removeChild(nextSibling);
      } while (nextSibling !== pins.end);
      pins = createFragment(document, html);
      parentNode.replaceChild(pins.fragment, start);
      return pins;
    }

    // if there's some binding dependent to a method it will update
    // the node whenever one bound parameter of the method changes
    function boundTransformer(source, autobots, bindings, method, keys, document, isHTML) {
      var
        pins = null,
        node = isHTML ? null : document.createTextNode('')
      ;
      keys.split(comma).forEach(optimusPrime, {
        autobots: autobots,
        bindings: bindings,
        method: source[method],
        source: source,
        onUpdate: isHTML ?
          function (value) {
            pins = pins ?
              updatePins(document, pins, value) :
              createFragment(document, value);
          } :
          function (value) {
            node.nodeValue = value;
          }
      });
      return node || pins.fragment;
    }

    // from a list of property names, returns an Array of values
    function getArgs() {/* jshint validthis:true */
      var a = [], i = 0;
      while (i < arguments.length) a[i] = this[arguments[i++]];
      return a;
    }

    // it controls all autobots, trying to defeat
    // all decepticons attack through their callbacks
    function optimusPrime(key, i, args) {/* jshint validthis:true */
      var
        autobots = this.autobots,
        bindings = this.bindings,
        source = this.source,
        method = this.method,
        onUpdate = this.onUpdate,
        setter = setGetSetIfAvailable(bindings, source, key).set,
        invoke = !!setter
      ;
      autobots[key] = source[key];
      dP(bindings, key, createGetSet(
        function get() { return autobots[key]; },
        function set(value) {
          autobots[key] = value;
          onUpdate(method.apply(bindings, getArgs.apply(autobots, args)));
          if (invoke) setter.call(bindings, value);
        }
      ));
      onUpdate(method.apply(bindings, getArgs.apply(autobots, args)));
    }

    // plain borrowed from twemoji ... but that's my code anyway ^_^
    function grabAllTextNodes(node, allText) {
      var
        childNodes = node.childNodes,
        length = childNodes.length,
        subnode,
        nodeType;
      while (length--) {
        subnode = childNodes[length];
        nodeType = subnode.nodeType;
        if (nodeType === 3) allText.push(subnode);
        else if (nodeType === 1 && !ignore.test(subnode.nodeName)) {
          grabAllTextNodes(subnode, allText);
        }
      }
      return allText;
    }

    // used to understand if and how to dispatch events
    function convertShenanigansToNumber(i) {
      switch (true) {
        case typeof i === 'number': return i < 0 ? -1 : i;
        case i: return 133;
        default: return -1;
      }
    }

    // set a binding property to a generic Custom Element
    function setBindings(ce, bindings) {
      dP(ce, 'bindings', {
        configurable: true,
        enumerable: true,
        writable: false,
        value: bindings
      });
    }

    // if the source had a getter/setter and
    // the target hasn't one descriptor yet
    // will copy such descriptor to not loose it
    // once it's re-configured
    function setGetSetIfAvailable(target, source, key) {
      var descriptor;
      if (hOP.call(target, key)) {
        descriptor = gOPD(target, key);
      } else {
        descriptor = gOPD(source, key) || whatToObserve;
        if (descriptor.set) {
          dP(target, key, descriptor);
        }
      }
      return descriptor;
    }

    return {

      // if the template is the same per each component
      // or there are common bindings, it does everything once created
      init: function () {
        if (this.template || this.bindings) this.createBindings(this);
      },

      // but it's also possible to lazy attach bindings later on
      // please not ce.dropBindings() will be called if already present
      createBindings: function (info) {

        // if it's been invoked multiple times, clean  up all the things
        if (hOP.call(this, DROP_BINDINGS)) this[DROP_BINDINGS]();

        // if the template is there but the node has no content
        // meaning it has been probably created via JS
        // it will inject the template right away
        if (info.template && !trim.call(this.innerHTML)) {
          this.innerHTML = info.template;
        }

        var
          // shortcut and reference
          self = this,
          // grab current document or use the global one
          document = self.ownerDocument || document,
          // used to define defaults, actually optional
          bindings = info.bindings || {},
          // will find all possible nodes with one-way bindings
          textNodes = grabAllTextNodes(self, []),
          // holds singular properties values related to nodes or attributes
          autobots = create(null),
          // maps DOM attribute names => bindings properties name
          map = create(null),
          // will be actually the exported binding object
          values = create(info.bindings || null),
          // grab all nodes with some [data-bind="value:prop"] info
          attributes = self.queryAll('[data-bind]'),
          // used as every DOM_ATTR_MODIFIED handler
          dAM = function (e) {
            var key = e.attrName, previous = state;
            state = STATE_EVENT;
            values[map[key]] = e.currentTarget[GET_ATTRIBUTE](key);
            state = previous;
          },
          // will be updated later on if a MutationObserver is needed
          setMO = false,
          // default state
          state = STATE_OFF,
          // NOTIFICATIONS
          // if bindings are dispatched, figure out in which frequency
          dispatchDelay = convertShenanigansToNumber(
            info.dispatchBindings || this.dispatchBindings
          ),
          // internal boolean flag, if dispatchDelay < 0 don't
          dispatchBindings = -1 < dispatchDelay,
          // in case there are bindings to dispatch, use a storage to filter notifications
          tobeNotified = dispatchBindings && create(null),
          // used to schedule and clean up notifications
          // the value 0 means ASAP and in that case rAF is used instead of setTimeout
          dispatcher = dispatchBindings && function (key) {
            delete tobeNotified[key];
            self.dispatchEvent(new CustomEvent('bindingChanged', {detail: {
              key: key,
              value: values[key]
            }}));
          },
          // schedules the dispatcher accordingly with the delay
          // if the dispatchDelay is 0 will use rAF instead (ASAP)
          dispatchScheduler = dispatchDelay ?
            function (key) {
              if (key in tobeNotified) clearTimeout(tobeNotified[key]);
              tobeNotified[key] = setTimeout(dispatcher, dispatchDelay, key);
            } :
            function (key) {
              if (key in tobeNotified) cancelAnimationFrame(tobeNotified[key]);
              tobeNotified[key] = requestAnimationFrame(function () {
                dispatcher(key);
              });
            },
          // whenever it's dropped and nothing else should happe in this scope
          dropped = false,
          // will be eventually used as MutationObserver instance
          mo
        ;

        dP(self, DROP_BINDINGS, {
          configurable: true,
          value: function () {
            var key;
            // if already dropped get out
            if (dropped) return;
            // flag it as dropped
            dropped = true;
            // clean up all scheduled callbacks
            if (dispatchBindings) {
              for (key in tobeNotified) {
                if (dispatchDelay) clearTimeout(tobeNotified[key]);
                else cancelAnimationFrame(tobeNotified[key]);
                delete tobeNotified[key];
              }
            }
            // clean up all info, getters, setters, etc
            for (key in autobots) delete autobots[key];
            for (key in map) delete map[key];
            for (key in values) delete values[key];
            setBindings(self, {});
            // disconnect the MutationObserver
            if (setMO) mo.disconnect();
            // or remove  all listeners
            else if (hasDAM) attributes.forEach(function (el) {
              off(el, DOM_ATTR_MODIFIED, dAM);
            });
            // or drop the fake setAttribute method
            else attributes.forEach(function (el) {
              delete el[SET_ATTRIBUTE];
            });
          }
        });

        // loop over all text nodes
        textNodes.forEach(function (node) {
          var
            isHTML,
            k, m, j, l,
            i = 0,
            value = node.nodeValue,
            nodes = [],
            bound = [],
            parentNode = node.parentNode,
            descriptor
          ;
          while ((m = oneWay.exec(value))) {
            j = m.index;
            l = m[0].length;
            nodes.push(value.slice(i, j));
            bound.push(value.substr(j + 2, l - 4));
            i = j + l;
          }
          // and in case there was some binding in the wild
          if (bound.length) {
            // put last part of the loop in the list of nodes
            nodes.push(value.slice(i));
            // and append all of them
            nodes.forEach(function (text, i) {
              // let's ignore empty text nodes
              if (text.length) parentNode.insertBefore(
                document.createTextNode(text),
                node
              );
              // bound nodes are always after regular
              // unless we are at the end of the list
              if (i < bound.length) {
                k = trim.call(bound[i]);
                // check if this is an HTML intent
                isHTML = oneWayHTML.test(k);
                  if (isHTML) k = k.slice(1, -1);
                if ((m = decepticons.exec(k))) {
                  parentNode.insertBefore(
                    boundTransformer(
                      bindings, autobots, values, m[1], m[2], document, isHTML
                    ),
                    node
                  );
                } else {
                  setGetSetIfAvailable(values, bindings, k);
                  parentNode.insertBefore(
                    isHTML ?
                    boundFragmentNode(
                      values, k,
                      document,
                      notNull(bindings[k], ''),
                      dispatchBindings,
                      dispatchScheduler
                    ) :
                    boundTextNode(
                      values, k,
                      document.createTextNode(
                        notNull(bindings[k], '')
                      ),
                      dispatchBindings,
                      dispatchScheduler
                    ),
                    node
                  );
                }
              }
            });
            // drop current node
            node.remove();
          }
        });

        attributes.forEach(function (el) {

          var
            setAttribute = el[SET_ATTRIBUTE],
            fakeSetAttribute = function (key, value) {
              var previous = state;
              state = STATE_EVENT;  // it's a white lie to simulate DAM
              setAttribute.call(this, key, value);
              // update only known values and only if the context is the right one
              // remember: we have one values per component shared only
              // with its own sub nodes. If somebody .call via different context
              // we don't want the values to be updated
              if (key in map && this === el) values[map[key]] = value;
              state = previous;
            }
          ;

          // KnockOut style, data-bind accepts a comma separated list of key/value pairs
          el[GET_ATTRIBUTE]('data-bind')
          .split(comma)
          .filter(function (info, i, all) {
            // sanitizes functions with possible multiple arguments
            if (info.indexOf('(') > 0 && info.indexOf(')') < 0) {
              all[i + 1] = info + ',' + all[i + 1];
              return false;
            }
            return true;
          }).forEach(function (info, i) {

            var
              // these pairs can optionally be separated via `:`
              pair = info.split(colon),
              // so that we always have the related element attribute name
              key = pair[0],
              // and eventually the corresponding property name on the binding
              value = pair[1] || key,
              // is this property is one of those "magic" properties ?
              // (value for inputs, selectedIndex for selects, checked for checkbox, etc)
              direct = key in el,
              m = pair[1] && decepticons.exec(value),
              handler,
              setter,
              descriptor,
              onAttached,
              onDetached,
              hasSet,
              v
            ;

            // one way binding, the attribute does not trigger changes in values
            // but it gets updated every time one of its parameters changes
            if (m) {
              m[2].split(comma).forEach(optimusPrime, {
                autobots: autobots,
                bindings: values,
                method: bindings[m[1]],
                source: bindings,
                onUpdate: direct ?
                  function (value) { el[key] = directMethod(values, value); } :
                  function (value) { setAttribute.call(el, key, value); }
              });
            } else {
              // handy to bring back property name from an attribute one
              map[key] = value;
              // if value has already a setter, don't loose it in the process
              setter = setGetSetIfAvailable(values, bindings, value).set;
              // in case there is a setter, we need to keep that "in mind"
              hasSet = !!setter;
              // if it's a direct property ...
              if (direct) {
                // we can simply set it as such using provided defaults
                if (hOP.call(bindings, value)) el[key] =
                  directMethod(values, bindings[value]);
                // console.log(gOPD(el, 'key'));
                // whenever we set such property via exported bindings
                dP(values, value, createGetSet(
                  // we either return it directly
                  function get() { return el[key]; },
                  // or we set it directly
                  function set(v) {
                    var previous = state;
                    state = STATE_DIRECT;
                    // console.log('direct', previous, state);
                    switch (previous) {
                      case STATE_OFF:
                      case STATE_EVENT:
                        el[key] = v;
                        if (dispatchBindings) dispatchScheduler(value);
                        break;
                    }
                    // if there was already a setter
                    // we should probably invoke it
                    if (hasSet) setter.call(values, v);
                    state = previous;
                  }
                ));
                // in few specific cases where the input could change via UI
                // we should update exported bindings property too
                handler = function (e) {
                  if (dropped) return off(el, e.type, handler);
                  var previous = state;
                  state = STATE_EVENT;
                  values[value] = el[key];
                  state = previous;
                };
                switch (key) {
                  case 'value':
                    on(el, 'input', handler);
                  /* falls through */
                  case 'checked':
                  /* falls through */
                  case 'selectedIndex':
                    on(el, 'change', handler);
                    break;
                }
                // we also would like to do the same in case
                // somebody directly changes the input.value
                // or the selectedIndex
                descriptor = getInterceptor(el, key);
                // if we can reuse the descriptor, we're better off this way
                if (descriptor) {
                  direct = hOP.call(el, key);
                  dP(el, key, {
                    configurable: true,
                    enumerable: descriptor.enumerable,
                    // get it as it's supposed to be get
                    get: descriptor.get,
                    // use the descriptor once set to update the element value
                    // and also update exported bindings property
                    set: function (v) {
                      if (dropped) return direct ?
                        dP(el, key, descriptor) : delete el[key];
                      var previous = state;
                      state = STATE_ATTRIBUTE;
                      descriptor.set.call(el, v);
                      values[value] = v;
                      state = previous;
                    }
                  });
                }
                // if we cannot reuse the inherited descriptor
                // we unfortunately need some utterly ugly polling fallback
                else {
                  // polling seems to be the best option
                  v = el[key];
                  // each time the following runs ...
                  handler = function () {
                    // don't reschedule if dropped
                    if (dropped) return;
                    // if direct property access is different
                    // from the known value
                    if (el[key] !== v) {
                      // we update the value
                      // and the exported bindings
                      var previous = state;
                      state = STATE_ATTRIBUTE;
                      v = el[key];
                      values[value] = v;
                      state = previous;
                    }
                    // check again ASAP
                    i = schedule(handler);
                  };
                  // in order to have a not so greedy schedule
                  // let's disable scheduling when the component
                  // is not even on the DOM
                  // TODO: should this actually run regardless?
                  onAttached = self[ON_ATTACHED];
                  onDetached = self[ON_DETACHED];
                  dP(self, ON_ATTACHED, {
                    configurable: true,
                    value: function () {
                      if (!dropped) handler(cancel(i));
                      if (onAttached) onAttached.apply(el, arguments);
                    }
                  });
                  dP(self, ON_DETACHED, {
                    configurable: true,
                    value: function () {
                      if (!dropped) cancel(i);
                      if (onDetached) onDetached.apply(el, arguments);
                    }
                  });
                  // let's start checking
                  handler();
                }
              }
              // here we are in setAttribute land
              else {
                // we can use the native method to set default value, if any
                if (hOP.call(bindings, value)) setAttribute.call(el, key, bindings[value]);
                // now we can set a different operation to update exported bindings
                dP(values, value, createGetSet(
                  // we use getAttribute when accessed
                  function get() { return el[GET_ATTRIBUTE](key); },
                  // and we use native setAttribute when changed
                  function set(v) {
                    var previous = state;
                    state = STATE_ATTRIBUTE;
                    // console.log('attribute', previous, state);
                    switch (previous) {
                      case STATE_OFF:
                      case STATE_DIRECT:
                        if (hasMo) mo.disconnect();
                        else if(hasDAM) off(el, DOM_ATTR_MODIFIED, dAM);
                        setAttribute.call(el, key, v);
                        if (dispatchBindings) dispatchScheduler(value);
                        if (hasMo) mo.observe(self, whatToObserve);
                        else if(hasDAM) on(el, DOM_ATTR_MODIFIED, dAM);
                        break;
                    }
                    // here again, if there was already a setter
                    // we should probably invoke it
                    if (hasSet) setter.call(values, v);
                    state = previous;
                  }
                ));
                // if we need to know about attributes
                // and MutationObserver is available
                if (hasMo) setMO = true; // let's use it
                // otherwise if DOMAttributeModified works
                // let's set a listener
                else if (hasDAM) on(el, DOM_ATTR_MODIFIED, dAM);
                // otherwise let's set once the  new fake method
                // it will simulate an event as if it was a DOMAttrModified
                else if (el[SET_ATTRIBUTE] !== fakeSetAttribute){
                  dP(el, SET_ATTRIBUTE, {
                    configurable: true,
                    value: fakeSetAttribute
                  });
                }
              }
            }
          });
          // if it's' a nested element avoid parsing from parent containers
          el.removeAttribute('data-bind');
        });

        // if MutationObserver is available and
        // if there was at least one attribute to listen to
        if (setMO) {
          // we can recreate a MutationObserver
          mo = new MO(function (records) {
            var previous = state;
            state = STATE_EVENT;
            // console.log('Mutation Observer', previous, state);
            for (var key, r, i = 0; i < records.length; i++) {
              r = records[i];
              // if it's about an attribute
              if (r.type === 'attributes') {
                key = r.attributeName;
                // and there is a key to take care of
                // and such key is also well known
                /* jshint eqnull:true */
                if (key != null && key in map) {
                  // we can update the value
                  values[map[key]] = r.target[GET_ATTRIBUTE](key);
                }
              }
            }
            state = previous;
          });
          // let's observe the node and its subnodes
          mo.observe(self, whatToObserve);

          /* // TODO: should mo stop listening when offline ?
          dP(self, 'attachedCallback', {
            configurable: true,
            value: function () {
              mo.observe(self, whatToObserve);
              if (onAttached) onAttached.apply(self, arguments);
            }
          });
          dP(self, 'detachedCallback', {
            configurable: true,
            value: function () {
              mo.disconnect();
              if (onDetached) onDetached.apply(self, arguments);
            }
          });
          //*/

        }

        // values object is the one exported as bindings
        setBindings(self, values);

        return self;

      }
    };

  }(Object))
});

var com;
(function (com) {
    var ciplogic;
    (function (ciplogic) {
        var callbacks = [], nextTickPointer = internalNextTick, nextTickFunction = typeof process != "undefined" && typeof process.nextTick == "function" ? process.nextTick : setTimeout;
        function nextTick(callback) {
            nextTickPointer(callback);
        }
        ciplogic.nextTick = nextTick;
        function internalNextTick(callback) {
            callbacks.push(callback);
            //setTimeout(runTicks, 0);
            nextTickFunction(runTicks, 0);
        }
        function addCallback(callback) {
            callbacks.push(callback);
        }
        function runTicks() {
            var fn;
            // while running ticks, adding new ticks is not needed to add a new setTimeout,
            // thus improving the performance quite a bit.
            try {
                nextTickPointer = addCallback;
                // FIXME: some yield should be done every 50-100 msecs
                while (fn = callbacks.shift()) {
                    fn.apply(undefined, []);
                }
            }
            finally {
                nextTickPointer = internalNextTick;
            }
        }
    })(ciplogic = com.ciplogic || (com.ciplogic = {}));
})(com || (com = {}));

var com;
(function (com) {
    var ciplogic;
    (function (ciplogic) {
        /**
         * Iterates over all the elements in the iterable, calling the callback on each one.
         * Basically poor man's `Array.forEach()`
         */
        function forEach(iterable, callback) {
            for (var i = 0; i < iterable.length; i++) {
                callback(iterable[i], i);
            }
        }
        /**
         * A promise can be in any of these states. FULFILLED and REJECTED are final states for a promise.
         */
        var PromiseState;
        (function (PromiseState) {
            PromiseState[PromiseState["FULFILLED"] = 0] = "FULFILLED";
            PromiseState[PromiseState["REJECTED"] = 1] = "REJECTED";
            PromiseState[PromiseState["PENDING"] = 2] = "PENDING";
        })(PromiseState || (PromiseState = {}));
        /**
         * <p>A promise follow up is a set of callbacks, followed by the next promise that are
         * registered on the "then" method of the Promise.</p>
         * <p>The callback function for onFulfill, or onReject will be called at most once as per
         * Promises spec.</p>
         */
        var PromiseFollowUp = (function () {
            function PromiseFollowUp() {
                this.callbacks = [null, null];
                this.promise = null;
            }
            return PromiseFollowUp;
        })();
        /**
         * <p>A promise represents the eventual result of an asynchronous operation. The primary way
         * of interacting with a promise is through its then method, which registers callbacks to
         * receive either a promise’s eventual value or the reason why the promise cannot be fulfilled.</p>
         * <p>This implementation is fully compatible with the specification from: http://promisesaplus.com/,
         * and passes all the tests defined here: https://github.com/promises-aplus/promises-tests.</p>
         *
         * @inmodule "core-promise"
         */
        var CorePromise = (function () {
            function CorePromise(executor) {
                var _this = this;
                if (!executor) {
                    throw new Error("You need an executor(resolve, reject) to be passed to " +
                        "the constructor of a Promise");
                }
                if (typeof this != "object") {
                    throw new TypeError("The this object for a Promise must be an object.");
                }
                if (this instanceof Number || this instanceof String || this instanceof Date || this instanceof Boolean) {
                    throw new TypeError("The this object for a Promise must be an object.");
                }
                if (typeof this._state != "undefined") {
                    throw new TypeError("Already constructed promise passed as this.");
                }
                if (typeof executor != "function") {
                    throw new TypeError("Executor must be a callable object.");
                }
                this._followUps = [];
                this._state = PromiseState.PENDING;
                try {
                    executor(function (r) {
                        CorePromise.resolvePromise(_this, r);
                    }, function (e) {
                        _this._reject(e);
                    });
                }
                catch (e) {
                    this._reject(e);
                }
            }
            CorePromise.prototype.then = function (onFulfill, onReject) {
                var followUp = new PromiseFollowUp();
                if (typeof onFulfill === "function") {
                    followUp.callbacks[PromiseState.FULFILLED] = onFulfill;
                }
                if (typeof onReject === "function") {
                    followUp.callbacks[PromiseState.REJECTED] = onReject;
                }
                followUp.promise = new CorePromise(function (fulfill, reject) { });
                this._followUps.push(followUp);
                this._notifyCallbacks();
                return followUp.promise;
            };
            CorePromise.prototype.catch = function (onReject) {
                return this.then(undefined, onReject);
            };
            /**
             * Always permits adding some code into the promise chain that will be called
             * irrespective if the chain is successful or not, in order to be used similarily
             * with a finally block.
             * @param always
             */
            CorePromise.prototype.always = function (fn) {
                return this.then(function (result) {
                    fn.apply(undefined);
                    return result;
                }, function (reason) {
                    fn.apply(undefined);
                    throw reason;
                });
            };
            CorePromise.resolve = function (x) {
                if (typeof this != "function") {
                    throw new TypeError("The this of Promise.resolve must be a constructor.");
                }
                if (x instanceof CorePromise) {
                    return x;
                }
                var result = new this(function (fulfill, reject) {
                    CorePromise.resolvePromise({
                        _fulfill: fulfill,
                        _reject: reject
                    }, x);
                });
                return result;
            };
            /**
             * The Promise.all(iterable) method returns a promise that resolves when all of the promises
             * in the iterable argument have resolved.
             * @param {Array<Promise<any>>} args
             * @returns {Promise<Iterable<T>>}
             */
            CorePromise.all = function (iterable) {
                if (typeof this != "function") {
                    throw new TypeError("The this of Promise.all must be a constructor.");
                }
                if (!iterable || typeof iterable.length == "undefined") {
                    return CorePromise.reject(new TypeError("Passed a non iterable to Promise.all(): " + typeof iterable));
                }
                if (iterable.length == 1) {
                    return CorePromise.resolve(iterable[0])
                        .then(function (it) { return [it]; });
                }
                if (iterable.length == 0) {
                    return CorePromise.resolve([]);
                }
                return new this(function (resolve, reject) {
                    var unresolvedPromisesCount = iterable.length, resolvedValues = new Array(iterable.length);
                    forEach(iterable, function (it, i) {
                        CorePromise.resolve(it).then(function (value) {
                            resolvedValues[i] = value;
                            unresolvedPromisesCount--;
                            if (unresolvedPromisesCount == 0) {
                                resolve(resolvedValues);
                            }
                        }, reject);
                    });
                });
            };
            /**
             * Create a new promise that is already rejected with the given value.
             */
            CorePromise.reject = function (reason) {
                if (typeof this != "function") {
                    throw new TypeError("The this of Promise.reject must be a constructor.");
                }
                return new this(function (fulfill, reject) {
                    reject(reason);
                });
            };
            /**
             * The Promise.race(iterable) method returns the first promise that resolves or
             * rejects from the iterable argument.
             * @param {Array<Promise<any>>} args
             * @returns {Promise<Iterable<T>>}
             */
            CorePromise.race = function (iterable) {
                if (typeof this != "function") {
                    throw new TypeError("The this of Promise.race must be a constructor.");
                }
                if (!iterable || typeof iterable.length == "undefined") {
                    return CorePromise.reject(new TypeError("Passed a non iterable to Promise.race(): " + typeof iterable));
                }
                if (iterable.length == 1) {
                    return CorePromise.resolve(iterable[0])
                        .then(function (it) { return [it]; });
                }
                if (iterable.length == 0) {
                    return new this(function (resolve, reject) { });
                }
                // if any of the promises is already done, resolve them faster.
                for (var i = 0; i < iterable.length; i++) {
                    if (iterable[i] instanceof CorePromise && iterable[i]._state != PromiseState.PENDING) {
                        return iterable[i];
                    }
                }
                return new this(function (resolve, reject) {
                    var rejectedPromiseCount = 0;
                    for (var i = 0; i < iterable.length; i++) {
                        CorePromise.resolvePromise({
                            _fulfill: resolve,
                            _reject: reject
                        }, iterable[i]);
                    }
                });
            };
            CorePromise.resolvePromise = function (promise, x) {
                if (promise === x) {
                    throw new TypeError();
                }
                if ((typeof x !== "function") && (typeof x !== "object") || !x) {
                    promise._fulfill(x);
                    return;
                }
                if (x instanceof CorePromise) {
                    x.then(function (v) {
                        promise._fulfill(v);
                    }, function (r) {
                        promise._reject(r);
                    });
                    return;
                }
                var then;
                try {
                    then = x.then;
                }
                catch (e) {
                    promise._reject(e);
                    return;
                }
                if (!then && typeof x === "function") {
                    then = x;
                }
                if (typeof then === "function") {
                    var execute = true;
                    try {
                        then.call(x, function (value) {
                            if (execute) {
                                execute = false;
                                CorePromise.resolvePromise(promise, value);
                            }
                        }, function (reason) {
                            if (execute) {
                                execute = false;
                                promise._reject(reason);
                            }
                        });
                    }
                    catch (e) {
                        if (execute) {
                            promise._reject(e);
                        }
                    }
                }
                else {
                    promise._fulfill(x);
                }
            };
            CorePromise.prototype._transition = function (state, value) {
                if (this._state == PromiseState.PENDING) {
                    this._state = state;
                    this._value = value;
                    this._notifyCallbacks();
                }
            };
            CorePromise.prototype._fulfill = function (value) {
                this._transition(PromiseState.FULFILLED, value);
                return this;
            };
            CorePromise.prototype._reject = function (reason) {
                this._transition(PromiseState.REJECTED, reason);
                return this;
            };
            CorePromise.prototype._notifyCallbacks = function () {
                var _this = this;
                if (this._state !== PromiseState.PENDING) {
                    var followUps = this._followUps;
                    this._followUps = [];
                    com.ciplogic.nextTick(function () {
                        for (var i = 0; i < followUps.length; i++) {
                            var followUpPromise;
                            followUpPromise = followUps[i].promise;
                            try {
                                if (followUps[i].callbacks[_this._state] == null) {
                                    followUpPromise._transition(_this._state, _this._value);
                                }
                                else {
                                    var result = followUps[i].callbacks[_this._state].call(undefined, _this._value);
                                    CorePromise.resolvePromise(followUpPromise, result);
                                }
                            }
                            catch (e) {
                                followUpPromise._transition(PromiseState.REJECTED, e);
                            }
                        }
                    });
                }
            };
            return CorePromise;
        })();
        ciplogic.CorePromise = CorePromise;
    })(ciplogic = com.ciplogic || (com.ciplogic = {}));
})(com || (com = {}));

if (typeof window['Promise'] == "undefined") {
    window['Promise'] = com.ciplogic.CorePromise;
}
